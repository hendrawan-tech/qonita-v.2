<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');
Route::get('/product/{slug}', 'FrontendController@detailProduct');
Route::get('/cart', 'FrontendController@cart');
Route::post('/cart', 'FrontendController@saveToCart');
Route::post('/checkout', 'FrontendController@checkout');
Route::get('/catalog', 'FrontendController@catalog');

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/storage/link', function () {
    Artisan::call('storage:link');
});

Route::get('/access/block', 'BlockController@index');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'access']], function () {
    Route::get('/homes/index', 'Dashboard\HomeController@index');
    Route::get('/settings/profile/', 'Dashboard\SettingController@profile');
    Route::put('/settings/profile/{id}', 'Dashboard\SettingController@updateprofile');
    Route::get('/settings/password/', 'Dashboard\SettingController@password');
    Route::put('/settings/password/{id}', 'Dashboard\SettingController@updatepassword');
    Route::resource('/settings/general', 'Dashboard\GeneralController');
    Route::resource('/managements/menu', 'Dashboard\MenuController');
    Route::resource('/managements/submenu', 'Dashboard\SubmenuController');
    Route::resource('/managements/role', 'Dashboard\RolemenuController');
    Route::resource('/users/index', 'Dashboard\UserController');
    Route::resource('/posts/post', 'Dashboard\PostController');
    Route::resource('/posts/category', 'Dashboard\CategoryController');
    Route::resource('/posts/tag', 'Dashboard\TagController');
    Route::resource('/posts/comment', 'Dashboard\CommentController');
    Route::resource('/products/product', 'Dashboard\ProductController');
    Route::get('/products/product/{id_product}/photo/{id}', 'Dashboard\ProductController@photo');
    Route::post('/products/product/{id}/photo/add', 'Dashboard\ProductController@addphoto');
    Route::resource('/products/color', 'Dashboard\ColorController');
    Route::resource('/products/size', 'Dashboard\SizeController');
    Route::resource('/products/category', 'Dashboard\CategoryProductController');
    Route::resource('/payments/payment', 'Dashboard\PaymentController');
    Route::resource('/orders/order', 'Dashboard\OrderController');
});
