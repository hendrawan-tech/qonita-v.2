## Installation

- Clone this repository
- run command composer Install
- create file .env
- create your database
- configuration database file .env
- run command php artisan key:generate
- import database to your database, file sql in folder database > ecommerce.sql
- run command php artisan storage:link
- run command php artisan serve
- login dashboard on url http://127.0.0.1:8000/login
- email : admin@mail.id | password : bismillah

## Selamat Ngoding :v