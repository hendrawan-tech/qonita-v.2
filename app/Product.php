<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(CategoryProduct::class, 'category_id', 'id');
    }

    public function photo()
    {
        return $this->hasMany(PhotoProduct::class, 'product_id');
    }

    public function size()
    {
        return $this->hasMany(SizeProduct::class);
    }

    public function color()
    {
        return $this->hasMany(ColorProduct::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItems::class);
    }
}
