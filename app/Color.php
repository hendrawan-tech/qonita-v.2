<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $guarded = ['id'];

    public function color()
    {
        return $this->hasMany(ColorProduct::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItems::class);
    }
}
