<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItems;
use App\Payment;
use App\PhotoProduct;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class FrontendController extends Controller
{
    public function index()
    {
        $product = Product::take(3)->orderBy('id', 'DESC')->get();
        $gallery = PhotoProduct::take(3)->orderBy('id', 'DESC')->get();
        return view('index', compact('product', 'gallery'));
    }

    public function catalog()
    {
        $products = Product::orderBy('id', 'DESC')->paginate(10);
        return view('catalog', compact('products'));
    }

    public function detailProduct($slug)
    {
        $product = Product::where('slug', $slug)->first();
        $recomended = Product::take(3)->orderBy('id', 'DESC')->get();
        return view('detail', compact('product', 'recomended'));
    }

    public function cart(Request $request)
    {
        $temp = [];

        if ($request->has('order_id')) {
            $data = json_decode(Cookie::get('product'), true);

            foreach ($data as $key => $value) {
                if ($key != $request->cart_id) {
                    array_push($temp, $value);
                }
            }

            if (sizeof($temp) == 0) {
                Cookie::queue(Cookie::forget('product'));
                return redirect('/cart');
            }
            Cookie::queue('product',json_encode($temp));

            return redirect('/cart');
        }


        $total_price = 0;
        $data = json_decode(Cookie::get('product'), true);

        if (!Cookie::get('product')) {
            $payments = Payment::all();
            $data = "";
            return view('cart', compact('payments', 'data'));
        }

        foreach ($data as $key => $data) {
            $total_price += $data['total_price'];
        }

        $data =
            json_decode(Cookie::get('product'));

        $payments = Payment::all();

        return view('cart')->withData($data)->withTotal($total_price)->withPayments($payments);
    }

    public function saveToCart(Request $request)
    {
        $temp = [];

        if (Cookie::get('product')) {
            $data = [
                'id' => $request->id,
                'name' => $request->name,
                'quantity' => $request->quantity,
                'price' => (int) $request->price,
                'photo' => $request->photo,
                'behalf_of' => $request->behalf_of,
                'size_id' => $request->size_id,
                'color_id' => $request->color_id,
                'total_price' => $request->quantity * $request->price
            ];

            $temp = json_decode(Cookie::get('product'));

            array_push($temp, $data);

            $temp = json_encode($temp);
        } else {
            $data = [[
                'id' => $request->id,
                'name' => $request->name,
                'quantity' => $request->quantity,
                'price' => (int) $request->price,
                'photo' => $request->photo,
                'behalf_of' => $request->behalf_of,
                'size_id' => $request->size_id,
                'color_id' => $request->color_id,
                'total_price' => $request->quantity * $request->price
            ]];

            $temp = json_encode($data);
        }

        Cookie::queue('product', $temp);

        return redirect('cart');
    }

    public function checkout(Request $request)
    {
        if (!Cookie::get('product')) {
            return redirect('/product');
        }

        $total_price = 0;
        $data = json_decode(Cookie::get('product'), true);

        foreach ($data as $key => $data) {
            $total_price += $data['total_price'];
        }

        $data = json_decode(Cookie::get('product'), true);

        $code = 'OR' . random_int(10000, 99999);

        $order = $request->validate([
            'email' => 'required',
            'username' => 'required',
            'payment_id' => 'exists:payments,id',
            'phone' => 'required',
        ]);

        $unique = random_int(10, 999);
        $order['code'] = $code;
        $order['total'] = $total_price + $unique;
        $order['status'] = 1;

        $result = Order::create($order);

        foreach ($data as $key => $item) { 
            OrderItems::create([
                'product_id' => $item['id'],
                'quantity' => $request->quantity[$key],
                'order_id' => $result->id,
                'behalf_of' => $request->behalf_of[$key],
                'color_id' => $item['color_id'],
                'size_id' => $item['size_id']
            ]);

            $cekdata = OrderItems::where('id', $item['id'])->first();
            $total_price += $cekdata->price * (int)$request->quantity[$key];
        }

        Cookie::queue(Cookie::forget('product'));

        return view('confirmation')->withResult($result)->withUnique($unique);
    }
}
