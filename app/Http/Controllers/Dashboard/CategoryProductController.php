<?php

namespace App\Http\Controllers\Dashboard;

use App\CategoryProduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryProductController extends Controller
{
    public function index()
    {
        $categoryProduct = CategoryProduct::all();
        return view('dashboard.categoryProduct.index', compact('categoryProduct'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.categoryProduct.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'category' => 'required'
        ]);
        $data['slug'] = strtolower(str_replace(' ', '-', $data['category']));
        CategoryProduct::create($data);
        
        return redirect('/dashboard/products/category')->with('status', 'Category Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $categoryProduct
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryProduct $categoryProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $categoryProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryProduct $category)
    {
        return view('dashboard.categoryProduct.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $categoryProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryProduct $category)
    {
        $data = $request->validate([
            'category' => 'required'
        ]);
        $data['slug'] = strtolower(str_replace(' ', '-', $data['category']));
        $category->update($data);
        
        return redirect('/dashboard/products/category')->with('status', 'Category Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $categoryProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryProduct $category)
    {
        $category->delete();
        return redirect('/dashboard/products/category')->with('status', 'Category Deleted');
    }
}
