<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Tag;
use Illuminate\Http\Request;

use function GuzzleHttp\json_decode;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Helper::AccessSubmenu()) {
            $tag = Tag::all();
            return view('dashboard.tag.index', compact('tag'));
        } else {
            return view('dashboard.error');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'tag' => 'required',
        ]);

        
        foreach($data as $item) {
            $item = json_decode($item, true);
            for($i = 0; $i < count($item); $i++) {
                $slug = strtolower(str_replace(' ', '-', $item[$i]['value']));
                Tag::create([
                    'tag' => $item[$i]['value'],
                    'slug' => $slug
                ]);
            }
        }
        
        return redirect('/dashboard/posts/tag')->with('status', 'Tag Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();
        return redirect('/dashboard/posts/tag')->with('status', 'Tag ' . $tag->tag . ' Dihapus');
    }
}
