<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\User;
use App\Quote;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $quote = Quote::orderBy(DB::raw('RAND()'))->first();

        $pesanan = Order::count();
        $produk = Product::count();
        $proses = Order::where('status', 2)->count();
        $terkirim = Order::where('status', 3)->count();
        return view('dashboard.home.index', compact('quote', 'pesanan', 'produk', 'proses', 'terkirim'));
    }
}
