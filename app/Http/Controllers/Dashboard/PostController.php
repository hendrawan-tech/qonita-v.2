<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Post;
use App\PostCategory;
use App\PostTag;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Helper::AccessSubmenu()) {
            $post = Post::all();
            return view('dashboard.post.post', compact('post'));
        } else {
            return view('dashboard.error');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Helper::AccessSubmenu()->create == 1)
        {
            $categories = Category::all();
            $tag = Tag::all();
            return view('dashboard.post.add', compact('categories', 'tag'));
        } else {
            return view('dashboard.error');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = $request->validate([
            'tag_id' => 'required',
        ]);

        $data = $request->validate([
            'title' => 'required|min:5',
            'description' => 'required|min:5',
            'image' => 'required|file|between:0,2048|mimes:jpeg,jpg,png',
            'category_id' => 'required',
            'status' => 'required',
        ]);

        $data['slug'] = strtolower(str_replace(' ', '-', $data['title']));
        $data['user_id'] = Auth::user()->id;
        $data['view'] = 0;

        $filetype = $request->file('image')->extension();
        $text = Str::random(16) . '.' . $filetype;
        $data['image'] = Storage::putFileAs('postImage', $request->file('image'), $text);

        $post = Post::create($data);

        foreach($tag as $item) {
            for($i = 0; $i < count($item); $i++) {
                PostTag::create([
                    'tag_id' => $item[$i],
                    'post_id' => $post->id
                ]);
            }
        }

        return redirect('/dashboard/posts/post')->with('status', 'Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if(Helper::AccessSubmenu()->edit == 1)
        {
            $categories = Category::all();
            $tag = Tag::all();
            
            return view('dashboard.post.edit', compact('categories', 'tag', 'post'));
        } else {
            return view('dashboard.error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $tag = ['tag_id' => $request->tag_id];

        $data = $request->validate([
            'title' => 'required|min:5',
            'description' => 'required|min:5',
            'image' => 'file|between:0,2048|mimes:jpeg,jpg,png',
            'category_id' => 'required',
            'status' => 'required',
        ]);

        $data['slug'] = strtolower(str_replace(' ', '-', $data['title']));
        $data['user_id'] = Auth::user()->id;
        $data['view'] = 0;

        if ($request['image']) {
            Storage::delete($post->image);
            $filetype = $request->file('image')->extension();
            $text = Str::random(16) . '.' . $filetype;
            $data['image'] = Storage::putFileAs('postImage', $request->file('image'), $text);
        }

        $post->update($data);

        if($request->tag_id) {
            PostTag::where('post_id', $post->id)->delete();
        }

        if($request->tag_id) {
            foreach($tag as $item) {
                for($i = 0; $i < count($item); $i++) {
                    PostTag::create([
                        'tag_id' => $item[$i],
                        'post_id' => $post->id
                    ]);
                }
            }
        }

        return redirect('/dashboard/posts/post')->with('status', 'Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if(Helper::AccessSubmenu()->delete == 1)
        {
            Storage::delete($post->image);
            PostTag::where('post_id', $post->id)->delete();
            $post->delete();
            return redirect('/dashboard/posts/post')->with('status', 'Post Deleted');
        }
        else 
        {
            return view('dashboard.error');
        }
    }
}
