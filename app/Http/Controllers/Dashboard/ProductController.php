<?php

namespace App\Http\Controllers\Dashboard;

use App\CategoryProduct;
use App\Color;
use App\ColorProduct;
use App\Product;
use App\Http\Controllers\Controller;
use App\PhotoProduct;
use App\Size;
use App\SizeProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        return view('dashboard.product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = CategoryProduct::all();
        $size = Size::all();
        $color = Color::all();
        return view('dashboard.product.create', compact('category', 'size', 'color'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'status' => 'required',
            'price' => 'required|integer',
            'category_id' => 'required',
            'description' => 'required',
        ]);
        
        $data['slug'] = strtolower(str_replace(' ', '-', $data['name']));

        $result = Product::create($data);

        foreach($request->color as $color) {
            ColorProduct::create([
                'color_id' => $color,
                'product_id' => $result->id,
            ]);
        }

        foreach($request->size as $size) {
            SizeProduct::create([
                'size_id' => $size,
                'product_id' => $result->id,
            ]);
        }
        
        if ($files = $request->file('photo')) {
            foreach($files as $img) {
                $filetype = $img->extension();
                $text = Str::random(16) . '.' . $filetype;
                $img = Storage::putFileAs('postImage', $img, $text);
                PhotoProduct::create([
                    'photo' => $img,
                    'product_id' => $result->id,
                ]);
            }
        }

        return redirect('/dashboard/products/product')->with('status', 'Product Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('dashboard.product.detail', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $category = CategoryProduct::all();
        $size = Size::all();
        $color = Color::all();
        return view('dashboard.product.edit', compact('category', 'size', 'color', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->validate([
            'name' => 'required',
            'status' => 'required',
            'price' => 'required|integer',
            'category_id' => 'required',
            'description' => 'required',
        ]);
        
        $data['slug'] = strtolower(str_replace(' ', '-', $data['name']));

        $product->update($data);

        return redirect('/dashboard/products/product')->with('status', 'Product Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Storage::delete($product->image);
        $product->delete();
        return redirect('/dashboard/products/product')->with('status', 'Product Deleted');
    }

    public function photo($id_product, $id_image)
    {
        $image = PhotoProduct::where('id', $id_image)->first();
        Storage::delete($image->image);
        $image->delete();
        return redirect('/dashboard/products/product/' . $id_product . '/edit');
    }

    public function addphoto(Request $request, $id_product)
    {
        if ($files = $request->file('photo')) {
            foreach($files as $img) {
                $filetype = $img->extension();
                $text = Str::random(16) . '.' . $filetype;
                $img = Storage::putFileAs('postImage', $img, $text);
                PhotoProduct::create([
                    'photo' => $img,
                    'product_id' => $id_product,
                ]);
            }
        }
        return redirect('/dashboard/products/product/' . $id_product . '/edit');
    }
}
