<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    protected $guarded = ['id'];

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id', 'id');
    }

    public function post()
    {
        return $this->hasMany(Post::class, 'id', 'post_id');
    }
}
