<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $guarded = ['id'];

    public function size()
    {
        return $this->hasMany(SizeProduct::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItems::class);
    }
}
