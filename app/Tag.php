<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = ['id'];

    public function postTag()
    {
        return $this->hasMany(PostTag::class, 'tag_id', 'id');
    }
}
