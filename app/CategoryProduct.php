<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    protected $table = 'category_products';
    protected $guarded = ['id'];

    public function product()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }
}
