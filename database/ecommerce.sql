-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 28 Okt 2020 pada 01.59
-- Versi server: 5.7.24
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `access_menus`
--

CREATE TABLE `access_menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `access_menus`
--

INSERT INTO `access_menus` (`id`, `role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(2, 1, 2, '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(3, 1, 3, '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(4, 1, 4, '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(5, 1, 5, '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(6, 1, 6, '2020-10-26 20:59:06', '2020-10-26 20:59:06'),
(7, 1, 7, '2020-10-26 21:03:23', '2020-10-26 21:03:23'),
(8, 1, 8, '2020-10-26 21:03:23', '2020-10-26 21:03:23'),
(9, 2, 1, '2020-10-27 17:26:38', '2020-10-27 17:26:38'),
(13, 2, 5, '2020-10-27 17:26:38', '2020-10-27 17:26:38'),
(14, 2, 6, '2020-10-27 17:26:38', '2020-10-27 17:26:38'),
(15, 2, 7, '2020-10-27 17:26:38', '2020-10-27 17:26:38'),
(16, 2, 8, '2020-10-27 17:26:38', '2020-10-27 17:26:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `access_submenus`
--

CREATE TABLE `access_submenus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `submenu_id` bigint(20) UNSIGNED NOT NULL,
  `create` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `edit` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `delete` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `access_submenus`
--

INSERT INTO `access_submenus` (`id`, `role_id`, `submenu_id`, `create`, `edit`, `delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(2, 1, 2, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(3, 1, 3, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(4, 1, 4, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(5, 1, 5, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(6, 1, 6, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(7, 1, 7, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(8, 1, 8, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(9, 1, 9, '0', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(10, 1, 10, '1', '1', '1', '2020-10-26 17:09:33', '2020-10-26 21:03:23'),
(11, 1, 11, '1', '1', '1', '2020-10-26 20:59:06', '2020-10-26 21:03:23'),
(12, 1, 12, '1', '1', '1', '2020-10-26 20:59:47', '2020-10-26 21:03:23'),
(13, 1, 13, '1', '1', '1', '2020-10-26 21:03:23', '2020-10-26 21:03:23'),
(14, 1, 14, '1', '1', '1', '2020-10-26 21:03:23', '2020-10-26 21:03:23'),
(15, 2, 1, '1', '1', '1', '2020-10-27 17:26:38', '2020-10-27 17:28:14'),
(23, 2, 9, '0', '0', '0', '2020-10-27 17:26:38', '2020-10-27 17:28:14'),
(24, 2, 10, '0', '0', '0', '2020-10-27 17:26:38', '2020-10-27 17:28:14'),
(25, 2, 11, '1', '1', '1', '2020-10-27 17:26:38', '2020-10-27 17:28:14'),
(26, 2, 12, '1', '1', '1', '2020-10-27 17:26:38', '2020-10-27 17:28:14'),
(27, 2, 13, '0', '1', '1', '2020-10-27 17:26:38', '2020-10-27 17:28:14'),
(28, 2, 14, '1', '1', '1', '2020-10-27 17:26:38', '2020-10-27 17:28:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `category_products`
--

CREATE TABLE `category_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `category_products`
--

INSERT INTO `category_products` (`id`, `category`, `created_at`, `updated_at`) VALUES
(1, 'Hijab', '2020-10-26 23:19:33', '2020-10-27 00:21:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `generals`
--

CREATE TABLE `generals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondary_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gradient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tw` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `generals`
--

INSERT INTO `generals` (`id`, `logo`, `title`, `tagline`, `primary_color`, `secondary_color`, `gradient`, `ig`, `fb`, `yt`, `tw`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'postImage/X8aGsDEpNHZnKS4p.png', 'Qonita', 'Site Tagline', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-26 17:09:33', '2020-10-26 20:17:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `urutan` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menus`
--

INSERT INTO `menus` (`id`, `menu`, `icon`, `url`, `is_active`, `created_at`, `updated_at`, `urutan`) VALUES
(1, 'Dashboard', 'fa fa-home', '/dashboard/homes', '1', '2020-10-26 17:09:33', '2020-10-26 18:29:50', 1),
(2, 'Posts', 'fa fa-edit', '/dashboard/posts', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33', 3),
(3, 'Managements', 'fa fa-list', '/dashboard/managements', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33', 2),
(4, 'Users', 'fa fa-users', '/dashboard/users', '1', '2020-10-26 17:09:33', '2020-10-26 20:00:06', 7),
(5, 'Settings', 'fa fa-cogs', '/dashboard/settings', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33', 8),
(6, 'Products', 'fas fa-store', '/dashboard/products', '1', '2020-10-26 20:58:01', '2020-10-26 21:03:40', 4),
(7, 'Orders', 'fas fa-shopping-cart', '/dashboard/orders', '1', '2020-10-26 21:00:54', '2020-10-26 21:03:36', 5),
(8, 'Payments', 'fas fa-credit-card', '/dashboard/payments', '1', '2020-10-26 21:02:15', '2020-10-26 21:03:44', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_07_15_083658_create_roles_table', 1),
(5, '2020_07_15_083709_create_menus_table', 1),
(6, '2020_07_15_083720_create_submenus_table', 1),
(7, '2020_07_15_083747_create_access_submenus_table', 1),
(8, '2020_07_15_085750_add_role_to_users_table', 1),
(9, '2020_07_25_212525_create_access_menus_table', 1),
(10, '2020_08_01_160126_create_categories_table', 1),
(11, '2020_08_01_160210_create_posts_table', 1),
(12, '2020_08_06_162434_create_generals_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `total` varchar(15) DEFAULT NULL,
  `code` varchar(7) DEFAULT NULL,
  `payment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1 = pending, 2 = proses, 3 = success, 4 = cancel',
  `address` varchar(200) DEFAULT NULL,
  `code_pos` varchar(7) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` int(2) DEFAULT NULL,
  `behalf_of` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `payments`
--

INSERT INTO `payments` (`id`, `name`, `card_number`, `bank`, `created_at`, `updated_at`) VALUES
(1, 'Qonita', '309439928394', 'BCA', '2020-10-26 22:50:51', '2020-10-26 22:59:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1 = ready, 2 = not ready',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image2` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `price`, `category_product_id`, `image`, `status`, `created_at`, `updated_at`, `description`, `image2`, `image3`) VALUES
(1, 'Simple Hijab with pastel color', 'simple-hijab-with-pastel-color', '100000', 1, 'postImage/BWQcUDgrBPIbhhHI.jpeg', 2, '2020-10-27 00:26:22', '2020-10-27 18:21:06', '<p class=\"font-italic font-sm mt-4\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>', 'postImage/YSrMqZd3Ge488k5h.png', 'postImage/JqAxcGtdEolHZjq8.jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(2, 'Admin', '2020-10-27 17:26:38', '2020-10-27 17:26:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `submenus`
--

CREATE TABLE `submenus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `submenu` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `submenus`
--

INSERT INTO `submenus` (`id`, `menu_id`, `submenu`, `url`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Home', '/dashboard/homes/index', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(2, 2, 'All Post', '/dashboard/posts/post', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(3, 2, 'Categories', '/dashboard/posts/category', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(4, 2, 'Comments', '/dashboard/posts/comment', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(5, 3, 'Menu Management', '/dashboard/managements/menu', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(6, 3, 'Submenu Management', '/dashboard/managements/submenu', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(7, 3, 'Role Permission', '/dashboard/managements/role', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(8, 4, 'Manage Users', '/dashboard/users/index', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(9, 5, 'General', '/dashboard/settings/general', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(10, 5, 'Your Profile', '/dashboard/settings/profile', '1', '2020-10-26 17:09:33', '2020-10-26 17:09:33'),
(11, 6, 'All Product', '/dashboard/products/item', '1', '2020-10-26 20:58:41', '2020-10-26 20:58:53'),
(12, 6, 'Category Product', '/dashboard/products/category', '1', '2020-10-26 20:59:35', '2020-10-26 20:59:35'),
(13, 7, 'All Order', '/dashboard/orders/item', '1', '2020-10-26 21:01:24', '2020-10-26 21:01:24'),
(14, 8, 'All Payments', '/dashboard/payments/payment', '1', '2020-10-26 21:03:00', '2020-10-26 22:58:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`) VALUES
(1, 'Hendrawan', 'hendrawan@mail.com', NULL, NULL, '$2y$10$Hl1DyHIjhflPrCrnGcgKu.3Y.qpO.Ctf1vHIFwggk1sKUP4FXbdoK', NULL, '2020-10-26 17:09:33', '2020-10-26 17:09:33', 1),
(2, 'Admin', 'admin@mail.id', NULL, NULL, '$2y$10$a/iWRP8Z2az3D8RGGTpLp.uFbrvkRK4tFyrGve3.QqRA1iRCw3Pom', NULL, '2020-10-27 17:28:46', '2020-10-27 17:28:46', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `access_menus`
--
ALTER TABLE `access_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_menus_role_id_foreign` (`role_id`),
  ADD KEY `access_menus_menu_id_foreign` (`menu_id`);

--
-- Indeks untuk tabel `access_submenus`
--
ALTER TABLE `access_submenus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_submenus_role_id_foreign` (`role_id`),
  ADD KEY `access_submenus_submenu_id_foreign` (`submenu_id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `category_products`
--
ALTER TABLE `category_products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_id` (`payment_id`);

--
-- Indeks untuk tabel `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_product_id` (`category_product_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `submenus`
--
ALTER TABLE `submenus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `submenus_menu_id_foreign` (`menu_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `access_menus`
--
ALTER TABLE `access_menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `access_submenus`
--
ALTER TABLE `access_submenus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `category_products`
--
ALTER TABLE `category_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `generals`
--
ALTER TABLE `generals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `submenus`
--
ALTER TABLE `submenus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `access_menus`
--
ALTER TABLE `access_menus`
  ADD CONSTRAINT `access_menus_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `access_menus_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `access_submenus`
--
ALTER TABLE `access_submenus`
  ADD CONSTRAINT `access_submenus_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `access_submenus_submenu_id_foreign` FOREIGN KEY (`submenu_id`) REFERENCES `submenus` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`);

--
-- Ketidakleluasaan untuk tabel `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Ketidakleluasaan untuk tabel `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_product_id`) REFERENCES `category_products` (`id`);

--
-- Ketidakleluasaan untuk tabel `submenus`
--
ALTER TABLE `submenus`
  ADD CONSTRAINT `submenus_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
