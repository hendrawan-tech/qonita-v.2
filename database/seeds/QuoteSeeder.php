<?php

use App\Quote;
use Illuminate\Database\Seeder;

class QuoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '01-11-2020',
            'quote' => 'Janganlah jadi orang bermuka dua, tapi carilah kawan bermuka dua. Saat kamu kehilangan muka, bisa pinjam satu ke kawanmu.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '02-11-2020',
            'quote' => 'Mau gagal, mau sukses itu tidak penting yang penting berhasil.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '03-11-2020',
            'quote' => 'Aku benci sama orang yang sok tau, banyak ngomong tapi gak ngerti. Makanya aku diam, supaya aku tidak membenci diriku sendiri.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '04-11-2020',
            'quote' => 'Bila kamu jelek, jangan takut mencintai. Karena yang seharusnya takut adalah orang yang kamu cintai.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '05-11-2020',
            'quote' => 'Cinta adalah pengorbanan, tapi kalo pengorbanan mulu sih namanya penderitaan.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '06-11-2020',
            'quote' => 'Salah kalau banyak yang bilang hidup itu cuma satu kali. Yang benar, hidup itu setiap hari, dan mati hanya sekali.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '07-11-2020',
            'quote' => 'Ketika kamu bilang biar Tuhan yang membalas, yang harus kamu tau adalah bahwa yang kamu alami sekarang adalah balasan dari Tuhan.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '08-11-2020',
            'quote' => 'Kesuksesan diraih karena usaha keras. Usaha keras berawal dari kemauan kuat. Kemauan kuat terdorong oleh cita-cita tinggi. Cita-cita tinggi berawal dari mimpi. Mimpi terjadi karena tidur. Maka kalau kamu ingin sukses mari tidur.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '09-11-2020',
            'quote' => 'Kalau kamu cari yang ganteng aku mundur, kalau kamu cari yang kaya, aku juga mundur, tapi kalau kamu cari yang pinter Matematika Akulator.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '10-11-2020',
            'quote' => 'Kalau kamu stres memikirkan jalan keluar dari masalah kamu, kembalilah ke jalan masuk.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '11-11-2020',
            'quote' => 'Pesan buat sesama perokok, jangan pernah takut mati kalau merokok, kan masih ada korek.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '12-11-2020',
            'quote' => 'Jadikan masa lalu menjadi masa bodo.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '13-11-2020',
            'quote' => 'Orang yang tidak pernah berjuang tidak akan pernah merasakan kalah'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '14-11-2020',
            'quote' => 'Nyatanya, walau sering dianggap tidak higenis, makan pinggir jalan lebih aman daripada makan di tengah jalan.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '15-11-2020',
            'quote' => 'Orang tua yang baik adalah yang memberi contoh kepada anaknya. Orang tua yang bijak adalah bisa belajar dari anaknya'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '16-11-2020',
            'quote' => 'Wanita yang cantik bukanlah jaminan untuk hidup bahagia dan menyenangkan apalagi yang jelek.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '17-11-2020',
            'quote' => 'Seberat apapun pekerjaan akan terasa ringan jika tidak dikerjakan.'
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '18-11-2020',
            'quote' => "Yang paling penting bagi seorang Pemimpin adalah 'N' Karena tanpa 'N' Pemimpin hanyalah Pemimpi."
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '19-11-2020',
            'quote' => "Salah kalau banyak yang bilang hidup hanya sekali, yang benar hidup itu setiap hari dan mati hanya sekali."
        ]);
        Quote::create([
            'name' => 'Hendrawan',
            'tanggal' => '20-11-2020',
            'quote' => "Kalau Cinta tidak bisa memiliki untuk apa diperjuangkan, itu adalah pikiran yang salah total, karena sebenarnya  ketika kita memilikipun bukan berarti perjuangan itu selesai."
        ]);
    }
}
