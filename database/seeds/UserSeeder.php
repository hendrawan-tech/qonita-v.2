<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Hendrawan',
            'email' => 'hendrareansha@gmail.com',
            'telepon' => '+62 3982-2390-2399',
            'address' => 'Bondowoso',
            'password' => Hash::make('hendrawan'),
            'avatar' => 'https://lh3.googleusercontent.com/a-/AOh14Ggip-ifW3ieYpt2MSONPUVROCu7eZQ1ydNsveiAbQ=s96-c',
            'role_id' => 1,
            'google_id' => '113944289247095438178',
        ]);
    }
}
