<?php

use App\Submenu;
use Illuminate\Database\Seeder;

class SubmenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Submenu::create([
            'menu_id' => 1,
            'submenu' => 'Beranda',
            'url' => '/dashboard/homes/index',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 2,
            'submenu' => 'Postingan',
            'url' => '/dashboard/posts/post',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 2,
            'submenu' => 'Kategori',
            'url' => '/dashboard/posts/category',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 2,
            'submenu' => 'Tag',
            'url' => '/dashboard/posts/tag',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 2,
            'submenu' => 'Komentar',
            'url' => '/dashboard/posts/comment',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 3,
            'submenu' => 'Menu Management',
            'url' => '/dashboard/managements/menu',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 3,
            'submenu' => 'Submenu Management',
            'url' => '/dashboard/managements/submenu',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 3,
            'submenu' => 'Role Permission',
            'url' => '/dashboard/managements/role',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 4,
            'submenu' => 'Daftar Pengguna',
            'url' => '/dashboard/users/index',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 5,
            'submenu' => 'Umum',
            'url' => '/dashboard/settings/general',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 5,
            'submenu' => 'Profil Anda',
            'url' => '/dashboard/settings/profile',
            'is_active' => '1'
        ]);
        Submenu::create([
            'menu_id' => 5,
            'submenu' => 'Keamanan',
            'url' => '/dashboard/settings/password',
            'is_active' => '1'
        ]);
    }
}
