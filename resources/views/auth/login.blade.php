<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="{{Helper::general()->tagline}}">
  <meta name="author" content="http://hendrawan.tech">

  <title>Dashboard | {{Helper::general()->title}}</title>
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
  
  <link href="{{asset('backend/assets/css/pages/login/classic/login-3.css?v=7.0.5')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('backend/assets/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('backend/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css">
  <link rel="icon" href="{{asset(Helper::general()->favicon)}}">
</head>

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
    <div class="d-flex flex-column flex-root">
        <div class="login login-3 login-signin-on d-flex flex-row-fluid" id="kt_login">
            <div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid" style="background-image: url({{asset('backend/assets/media/bg/bg-1.jpg')}});">
                <div class="login-form text-center text-white p-7 position-relative overflow-hidden">
                    <div class="d-flex flex-center mb-15">
                        <a href="#">
                            <img src="{{asset(Helper::general()->logo)}}" class="max-h-100px" alt="" />
                        </a>
                    </div>
                    <div class="login-signin">
                        <div class="mb-20">
                            <h3>Masuk</h3>
                            <p class="opacity-60 font-weight-bold">Silahkan masuk dengan akun anda</p>
                        </div>
                        <form method="POST" action="{{ route('login') }}" class="form" id="kt_login_signin_form">
                            @csrf
                            <div class="form-group">
                                <input class="form-control h-auto text-white @error('email') is-invalid @enderror placeholder-white opacity-70 bg-dark-o-70 rounded-pill border-0 py-4 px-8 mb-5" type="text" placeholder="Email" name="email" value="{{old('email')}}" autocomplete="off" />
                                @error('email')
                                <div class="fv-plugins-message-container"><div data-field="username" data-validator="notEmpty" class="fv-help-block">{{$message}}</div></div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input class="form-control h-auto text-white @error('password') is-invalid @enderror placeholder-white opacity-70 bg-dark-o-70 rounded-pill border-0 py-4 px-8 mb-5" type="password" name="password" value="{{old('password')}}" placeholder="Password" name="password" />
                                @error('password')
                                <div class="fv-plugins-message-container"><div data-field="username" data-validator="notEmpty" class="fv-help-block">{{$message}}</div></div>
                                @enderror
                            </div>
                            <div class="form-group d-flex flex-wrap justify-content-between align-items-center px-8">
                                <div class="checkbox-inline">
                                  <label class="checkbox checkbox-outline checkbox-white text-white m-0">
                                    <input type="checkbox" name="remember" />
                                    <span></span>Ingat Saya
                                  </label>
                                </div>
                                <a href="javascript:;" id="kt_login_forgot" class="text-white font-weight-bold">Lupa Password ?</a>
                            </div>
                            <div class="form-group text-center mt-10">
                                <button id="kt_login_signin_submit" class="btn btn-pill btn-outline-white font-weight-bold opacity-90 px-15 py-3">Masuk</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>