@extends('dashboard.templates.master')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap py-5">
                @include('dashboard.templates.heading-card')
            </div>
            <div class="card-body">
                <form action="/dashboard/payments/payment" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4 mb-3">
                            <input type="text" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" placeholder="Atas Nama">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                        <div class="col-lg-4 mb-3">
                            <input type="text" name="bank" value="{{old('bank')}}" class="form-control @error('bank') is-invalid @enderror" placeholder="Nama Bank">
                            @error('bank')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                        <div class="col-lg-4 mb-3">
                            <input type="text" name="card_number" value="{{old('card_number')}}" class="form-control @error('card_number') is-invalid @enderror" placeholder="No Rekening">
                            @error('card_number')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection