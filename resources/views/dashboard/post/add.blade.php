@extends('dashboard.templates.master')
@section('content')

<form action="/dashboard/posts/post" enctype="multipart/form-data" method="POST">
    @csrf
    <div class="row">
        <div class="col-lg-9">
            <div class="card card-custom">
                <div class="card-header flex-wrap py-5">
                    @include('dashboard.templates.heading-card')
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Judul Postingan <span class="text-danger">*</span></label>
                                <input type="text" name="title" value="{{old('title')}}" class="form-control @error('title') is-invalid @enderror" placeholder="Masukkan Judul Postingan">
                                @error('title')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Deskripsi Postingan <span class="text-danger">*</span></label>
                                <textarea class="summernote" id="kt_summernote_1" name="description">{{old('description')}}</textarea>
                                @error('description')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card shadow mb-4">
                <div class="card-header flex-wrap py-5">
                    <h3 class="card-label">Terbitkan</h3>
                    <span class="d-block text-muted pt-2 font-size-sm">Utility</span></h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Kategori Postingan <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="kt_select2_1" name="category_id">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($categories as $m)
                                        <option value="{{$m->id}}" {{old('category_id') == $m->id ? 'selected' : ''}}>{{$m->category}}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Status Postingan <span class="text-danger">*</span></label>
                                <select class="form-control selectpicker" name="status">
                                    <option value="">Pilih Status</option>
                                    <option value="public" {{old('status') == 'public' ? 'selected' : ''}}>Public</option>
                                    <option value="pending" {{old('status') == 'pending' ? 'selected' : ''}}>Pending</option>
                                </select>
                                @error('status')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Tag Postingan <span class="text-danger">*</span></label>
                                <select class="form-control select2" id="kt_select2_11" multiple="multiple" name="tag_id[]">
                                    @foreach ($tag as $m)
                                        <option value="{{$m->id}}" {{old('tag_id') == $m->id ? 'selected' : ''}}>{{$m->tag}}</option>
                                    @endforeach
                                </select>
                                @error('tag')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 mb-3">
                            <div class="form-group">
                                <label for="">Foto <span class="text-danger">*</span></label>
                                <input type="file" id="input-file-now" name="image" class="dropify" />
                                @error('image')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <button class="btn btn-primary w-100" type="submit">Posting</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('backend/assets/css/dropify.min.css') }}">
@endpush

@push('scripts')
  <script src="{{asset('backend/assets/js/pages/crud/forms/editors/summernote.js')}}"></script>
  <script src="{{ asset('backend/assets/js/dropify.min.js') }}"></script>
  <script src="{{ asset('backend/assets/js/dropify.js') }}"></script>
  <script src="{{ asset('backend/assets/js/pages/crud/forms/widgets/select2.js') }}"></script>
@endpush