@extends('dashboard.templates.master')
@section('content')

<form action="/dashboard/products/size" enctype="multipart/form-data" method="POST">
    @csrf
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-custom">
                <div class="card-header flex-wrap py-5">
                    @include('dashboard.templates.heading-card')
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nama Ukuran <span class="text-danger">*</span></label>
                                <input type="text" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama Warna">
                                @error('name')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Ukuran<span class="text-danger">*</span></label>
                                <select class="form-control selectpicker @error('size') is-invalid @enderror" name="size">
                                    <option value="">Pilih Ukuran</option>
                                    <option value="S" {{old('size') == 'S' ? 'selected' : ''}}>S</option>
                                    <option value="M" {{old('size') == 'M' ? 'selected' : ''}}>M</option>
                                    <option value="L" {{old('size') == 'L' ? 'selected' : ''}}>L</option>
                                    <option value="XL" {{old('size') == 'XL' ? 'selected' : ''}}>XL</option>
                                </select>
                            </div>
                            @error('status')
                                <span class="text-danger ml-3">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-lg-12">
                            <button class="btn btn-primary" type="submit">Tambah</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection