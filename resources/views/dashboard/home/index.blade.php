@extends('dashboard.templates.master')
@section('content')
<div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
  <div class="alert-icon">
    <span class="svg-icon svg-icon-primary svg-icon-2x">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <rect x="0" y="0" width="24" height="24"/>
          <polygon fill="#000000" points="11 7 9 13 11 13 11 18 6 18 6 13 8 7"/>
          <polygon fill="#000000" opacity="0.3" points="19 7 17 13 19 13 19 18 14 18 14 13 16 7"/>
        </g>
      </svg>
    </span>
  </div>
  <div class="alert-text text-center">{{$quote->quote}}</div>
  <div class="alert-icon">
    <span class="svg-icon svg-icon-primary svg-icon-2x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <rect x="0" y="0" width="24" height="24"/>
          <polygon fill="#000000" transform="translate(16.500000, 12.500000) rotate(-180.000000) translate(-16.500000, -12.500000) " points="19 7 17 13 19 13 19 18 14 18 14 13 16 7"/>
          <polygon fill="#000000" opacity="0.3" transform="translate(8.500000, 12.500000) rotate(-180.000000) translate(-8.500000, -12.500000) " points="11 7 9 13 11 13 11 18 6 18 6 13 8 7"/>
      </g>
  </svg></span>
  </div>
</div>

<div class="row">
  <div class="col-xl-3">
    <!--begin::Stats Widget 13-->
    <a href="/dashboard/orders/order" class="card card-custom bg-white card-stretch gutter-b">
      <!--begin::Body-->
      <div class="card-body">
        <span class="svg-icon svg-icon-info svg-icon-3x ml-n1">
          <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg-->
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24" />
              <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
              <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
            </g>
          </svg>
          <!--end::Svg Icon-->
        </span>
        <div class="text-inverse-white font-weight-bolder font-size-h5 mb-2 mt-5">Jumlah Pesanan ({{$pesanan}})</div>
        <div class="font-weight-bold text-inverse-white font-size-sm">Dashboard, Order, Item</div>
      </div>
      <!--end::Body-->
    </a>
    <!--end::Stats Widget 13-->
  </div>
  <div class="col-xl-3">
    <!--begin::Stats Widget 14-->
    <a href="/dashboard/products/product" class="card card-custom bg-white card-stretch gutter-b">
      <!--begin::Body-->
      <div class="card-body">
        <span class="svg-icon svg-icon-primary svg-icon-3x ml-n1"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Shopping\Box3.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"/>
              <path d="M20.4061385,6.73606154 C20.7672665,6.89656288 21,7.25468437 21,7.64987309 L21,16.4115967 C21,16.7747638 20.8031081,17.1093844 20.4856429,17.2857539 L12.4856429,21.7301984 C12.1836204,21.8979887 11.8163796,21.8979887 11.5143571,21.7301984 L3.51435707,17.2857539 C3.19689188,17.1093844 3,16.7747638 3,16.4115967 L3,7.64987309 C3,7.25468437 3.23273352,6.89656288 3.59386153,6.73606154 L11.5938615,3.18050598 C11.8524269,3.06558805 12.1475731,3.06558805 12.4061385,3.18050598 L20.4061385,6.73606154 Z" fill="#000000" opacity="0.3"/>
              <polygon fill="#000000" points="14.9671522 4.22441676 7.5999999 8.31727912 7.5999999 12.9056825 9.5999999 13.9056825 9.5999999 9.49408582 17.25507 5.24126912"/>
          </g>
        </svg><!--end::Svg Icon--></span>
        <div class="text-inverse-white font-weight-bolder font-size-h5 mb-2 mt-5">Jumlah Produk ({{$produk}})</div>
        <div class="font-weight-bold text-inverse-white font-size-sm">Dashboard, Product, Data</div>
      </div>
      <!--end::Body-->
    </a>
    <!--end::Stats Widget 14-->
  </div>
  <div class="col-xl-3">
    <!--begin::Stats Widget 15-->
    <a href="" class="card card-custom bg-white card-stretch gutter-b">
      <!--begin::Body-->
      <div class="card-body">
          <span class="svg-icon svg-icon-warning svg-icon-3x ml-n1"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Communication\Clipboard-list.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <rect x="0" y="0" width="24" height="24"/>
                  <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"/>
                  <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"/>
                  <rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"/>
                  <rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"/>
                  <rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"/>
                  <rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"/>
                  <rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"/>
                  <rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"/>
              </g>
          </svg><!--end::Svg Icon--></span>
        <div class="text-inverse-white font-weight-bolder font-size-h5 mb-2 mt-5">Pesanan Diproses ({{$proses}})</div>
        <div class="font-weight-bold text-inverse-white font-size-sm">Dashboard, Order, Item</div>
      </div>
      <!--end::Body-->
    </a>
    <!--end::Stats Widget 15-->
  </div>
  <div class="col-xl-3">
    <!--begin::Stats Widget 15-->
    <a href="/dashboard/orders/order" class="card card-custom bg-white card-stretch gutter-b">
      <!--begin::Body-->
      <div class="card-body">
        <span class="svg-icon svg-icon-success svg-icon-3x ml-n1"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Communication\Clipboard-check.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"/>
              <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"/>
              <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"/>
              <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"/>
          </g>
      </svg><!--end::Svg Icon--></span>
        <div class="text-inverse-white font-weight-bolder font-size-h5 mb-2 mt-5">Pesanan Terkirim ({{$terkirim}})</div>
        <div class="font-weight-bold text-inverse-white font-size-sm">Dashboard, Order, Item</div>
      </div>
      <!--end::Body-->
    </a>
    <!--end::Stats Widget 15-->
  </div>
</div>
@endsection