@php
  $permission = Helper::permission();
  $url_submenu = '/' . Request::segment(1) . '/' . Request::segment(2) . '/' . Request::segment(3);
  $submenu = DB::select("SELECT * FROM submenus WHERE url = '$url_submenu'");
  $menu = '/' . Request::segment(1) . '/' . Request::segment(2);
  $menu = DB::select("SELECT menu FROM menus WHERE url = '$menu'");
@endphp

<div class="card-title">
  @if(Request::segment(4) == 'create')
    <h3 class="card-label">Tambah
    <span class="d-block text-muted pt-2 font-size-sm">Data {{$submenu[0]->submenu}}</span></h3>
  @elseif(Request::segment(5) == 'edit')
    <h3 class="card-label">Edit
    <span class="d-block text-muted pt-2 font-size-sm">Data {{$submenu[0]->submenu}}</span></h3>
  @else
    <h3 class="card-label">Semua 
    <span class="d-block text-muted pt-2 font-size-sm">Data {{$submenu[0]->submenu}}</span></h3>
  @endif
</div>
<div class="card-toolbar">
    @if ($permission !== null)
        @if ($permission->create == 1)
            @if (Route::current()->getName() == Request::segment(3) . '.index')
                <a href="{{$permission->url . '/create'}}" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <circle fill="#000000" cx="9" cy="15" r="6" />
                          <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                </span>Tambah Data</a>
            @endif
        @endif
    @endif  
</div>