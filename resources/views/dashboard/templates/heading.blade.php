@php
  $permission = Helper::permission();
  $url_submenu = '/' . Request::segment(1) . '/' . Request::segment(2) . '/' . Request::segment(3);
  $submenu = DB::select("SELECT submenu FROM submenus WHERE url = '$url_submenu'");
  $menu = '/' . Request::segment(1) . '/' . Request::segment(2);
  $menu = DB::select("SELECT menu FROM menus WHERE url = '$menu'");
@endphp

<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
  <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-center flex-wrap mr-2">
      <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">{{$submenu[0]->submenu}}</h5>
    </div>
    <div class="d-flex align-items-center">
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="/dashboard/homes/index" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#" class="text-muted">{{$menu[0]->menu}}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#" class="text-muted">{{$submenu[0]->submenu}}</a>
        </li>
        @if (Request::segment(4) == 'create')
        <li class="breadcrumb-item">
          <a href="#" class="text-muted">Tambah</a>
        </li>
        @elseif(Request::segment(5) == 'edit')
        <li class="breadcrumb-item">
          <a href="#" class="text-muted">Edit</a>
        </li>
        @endif
      </ul>
    </div>
  </div>
</div>