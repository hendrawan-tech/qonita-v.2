<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="{{Helper::general()->tagline}}">
  <meta name="author" content="http://hendrawan.tech">

  <title>Dashboard | {{Helper::general()->title}}</title>
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
  <link href="{{asset('backend/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('backend/assets/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('backend/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('backend/assets/css/themes/layout/header/base/light.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('backend/assets/css/themes/layout/aside/light.css')}}" rel="stylesheet" type="text/css">

  <link rel="icon" href="{{asset(Helper::general()->favicon)}}">
  
  @stack('styles')
</head>

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">

  <div id="kt_header_mobile" class="header-mobile align-items-center header-mobile-fixed">
    <a href="/">
      <h3>{{Helper::general()->title}}</h3>
    </a>
    <div class="d-flex align-items-center">
      <button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
        <span></span>
      </button>
    </div>
  </div>

  <div class="d-flex flex-column flex-root">
    <div class="d-flex flex-row flex-column-fluid page">
      @include('dashboard.templates.sidebar')
      <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
        @include('dashboard.templates.topbar')
        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
          @include('dashboard.templates.heading')
          <div class="d-flex flex-column-fluid">
            <div class="container">
              @yield('content')
            </div>
          </div>
        </div>

        <div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
          <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
            <div class="text-dark order-2 order-md-1">
              <span class="text-muted font-weight-bold mr-2">2020 © </span>
              <a href="http://hendrawan.tech" target="_blank" class="text-dark-75 text-hover-primary">Hendrawan</a>
            </div>
            <div class="nav nav-dark">
              <a href="http://hendrawan.tech" target="_blank" class="nav-link pl-0 pr-5">About</a>
              <a href="http://hendrawan.tech" target="_blank" class="nav-link pl-0 pr-5">Team</a>
              <a href="http://hendrawan.tech" target="_blank" class="nav-link pl-0 pr-0">Contact</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="exampleModalLg" tabindex="-1" role="dialog"
      aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Keluar dari {{Helper::general()->title}}</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&#xD7;</span></button>
              </div>
              <div class="modal-body">
                  <p><b>{{ Auth::user()->name }}</b>, apakah anda yakin ingin keluar?</p>
              </div>
              <div class="modal-footer">
                <button class="btn btn-success"
                      type="button" data-dismiss="modal">Batal</button>
                      <a class="btn btn-danger" href="" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Keluar</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="modal_delete">
    <div class="modal-dialog">
      <div class="modal-content" style="margin-top:100px;">
        <form action="" method="POST" name="formdelete">
          @method('delete')
          @csrf
          <div class="modal-header">
            <h4 class="modal-title" style="text-align:center;">Hapus Data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Apakah anda yakin ingin menghapus data ini?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-danger">Hapus</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
  
  <script src="{{asset('backend/assets/plugins/global/plugins.bundle.js')}}"></script>
  <script src="{{asset('backend/assets/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
  <script src="{{asset('backend/assets/js/scripts.bundle.js')}}"></script>

  <script type="text/javascript">
    function modal_konfir(url) {
      $('#modal_delete').modal('show', {
        backdrop: 'static'
      });
      document.formdelete.action = url;
    }
    
    function modal_konfir_custom(url, pesan) {
        $('#modal_konfirmasi').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('link_goto').setAttribute('href', url);
        document.getElementById('modal_pesan').innerHTML = pesan;
    }
  </script>

  @stack('scripts')
</body>

</html>
