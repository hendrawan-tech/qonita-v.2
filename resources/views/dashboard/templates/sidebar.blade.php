<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
  <div class="brand flex-column-auto" id="kt_brand">
    <a href="/dashboard/homes/index" class="brand-logo">
      <h5 class="text-white mt-1 font-weight-bold">{{Helper::general()->title}}</h5>
    </a>
  </div>
  <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
      <ul class="menu-nav">
        <div class="text-center">
          <div class="symbol symbol-60 symbol-circle symbol-xl-90">
            <div class="symbol-label" style="background-image:url({{asset(Auth::user()->avatar)}})"></div>
            <i class="symbol-badge symbol-badge-bottom bg-success"></i>
          </div>
          <h4 class="font-weight-bold my-2">{{Auth::user()->name}}</h4>
          <div class="text-muted mb-3">{{Auth::user()->role->role}}</div>
          <div class="separator separator-dashed m-5"></div>
        </div>

        @php
          $role_id = Auth::user()->role_id;
          $main_menu = '/' . Request::segment(1) . '/' . Request::segment(2);
          $main_submenu = '/' . Request::segment(1) . '/' . Request::segment(2) . '/' . Request::segment(3);
          $menu = DB::table('menus')
              ->join('access_menus', 'menus.id', '=', 'access_menus.menu_id')
              ->where('access_menus.role_id', $role_id)->orderBY('menus.sort', 'ASC')
              ->get();
        @endphp

        @foreach ($menu as $m)
          @php
            $submenu = DB::table('submenus')
            ->join('access_submenus', 'submenus.id', '=', 'access_submenus.submenu_id')
            ->where(['access_submenus.role_id' => $role_id, 'submenus.menu_id' => $m->menu_id])
            ->get();
          @endphp

          @if (count($submenu) == 1)
          <li class="menu-item {{$m->url == $main_menu ? 'menu-item-active' : ''}}" aria-haspopup="true">
            <a href="{{$submenu[0]->url}}" class="menu-link">
              {!! $m->icon !!}
              <span class="menu-text">{{$m->menu}}</span>
            </a>
          </li>
          @else
          <li class="menu-item menu-item-submenu {{$m->url == $main_menu ? 'menu-item-open' : ''}}" aria-haspopup="true" data-menu-toggle="hover">
            <a href="javascript:;" class="menu-link menu-toggle">
              {!! $m->icon !!}
              <span class="menu-text">{{$m->menu}}</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="menu-submenu">
              <i class="menu-arrow"></i>
              <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                  <span class="menu-link">
                    <span class="menu-text">{{$m->menu}}</span>
                  </span>
                </li>
                @foreach ($submenu as $sm)
                <li class="menu-item {{$sm->url == $main_submenu ? 'menu-item-active' : ''}}" aria-haspopup="true">
                  <a href="{{$sm->url}}" class="menu-link">
                    <i class="menu-bullet menu-bullet-dot">
                      <span></span>
                    </i>
                    <span class="menu-text">{{$sm->submenu}}</span>
                  </a>
                </li>
                @endforeach
              </ul>
            </div>
          </li>
          @endif
        @endforeach
      </ul>
    </div>
  </div>
</div>