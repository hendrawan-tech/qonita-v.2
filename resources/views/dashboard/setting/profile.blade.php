@extends('dashboard.templates.master')

@section('content')

@include('dashboard.templates.alert')

<div class="d-flex flex-row">
    <div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
        <div class="card card-custom card-stretch">
            <div class="card-body pt-10">
                <div class="text-center mb-10">
                    <div class="symbol symbol-60 symbol-circle symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                        <div class="symbol-label" style="background-image:url({{asset(Auth::user()->avatar)}})"></div>
                        <i class="symbol-badge bg-success"></i>
                    </div>
                    <h4 class="font-weight-bold my-2">{{ Auth::user()->name }}</h4>
                    <div class="text-muted mb-2">{{ Auth::user()->role->role }}</div>
                    <div class="mt-3">
                        <a href="{{Auth::user()->instagram}}" target="_blank" class="btn btn-icon btn-circle btn-light-instagram mr-2">
                            <i class="socicon-instagram"></i>
                        </a>
                        <a href="{{Auth::user()->facebook}}" target="_blank" class="btn btn-icon btn-circle btn-light-facebook mr-2">
                            <i class="socicon-facebook"></i>
                        </a>
                        <a href="{{Auth::user()->twitter}}" target="_blank" class="btn btn-icon btn-circle btn-light-twitter mr-2">
                            <i class="socicon-twitter"></i>
                        </a>
                        <a href="{{Auth::user()->youtube}}" target="_blank" class="btn btn-icon btn-circle btn-light-youtube mr-2">
                            <i class="socicon-youtube"></i>
                        </a>
                    </div>
                </div>
                <div class="separator separator-dashed m-5"></div>
                <div class="py-5">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Email:</span>
                        <span href="#" class="text-muted text-hover-primary">{{ Auth::user()->email }}</span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">No Telepon :</span>
                        <span class="text-muted">{{ Auth::user()->telepon }}</span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between">
                        <span class="font-weight-bold mr-2">Alamat:</span>
                        <span class="text-muted">{{ Auth::user()->address }}</span>
                    </div>
                </div>
                <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                    <div class="navi-item mb-2">
                        <a href="/dashboard/settings/profile" class="navi-link py-4 active">
                            <span class="navi-icon mr-2">
                                <span class="svg-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24" />
                                            <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                            <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                        </g>
                                    </svg>
                                </span>
                            </span>
                            <span class="navi-text font-size-lg">Informasi Pribadi</span>
                        </a>
                    </div>
                    
                    <div class="navi-item mb-2">
                        <a href="/dashboard/settings/password" class="navi-link py-4">
                            <span class="navi-icon mr-2">
                                <span class="svg-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3" />
                                            <path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3" />
                                            <path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3" />
                                        </g>
                                    </svg>
                                </span>
                            </span>
                            <span class="navi-text font-size-lg">Ganti Password</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="flex-row-fluid ml-lg-8">
        <form class="form" action="/dashboard/settings/profile/{{Auth::user()->id}}" method="POST" enctype="multipart/form-data">
        @method('put')
        @csrf
            <div class="card card-custom card-stretch">
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Informasi Primadi</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">Ubah informasi pribadi anda</span>
                    </div>
                    <div class="card-toolbar">
                        <button type="submit" class="btn btn-primary mr-2">Simpan Perubahan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Avatar</label>
                        <div class="col-lg-9 col-xl-6">
                            <input type="file" id="input-file-now" name="avatar" class="dropify" @if (Auth::user()->avatar) data-default-file="{{asset(Auth::user()->avatar)}}" @endif/>
                            @error('avatar')
                                <span class="text-danger mt-2">{{$message}}</span>
                            @enderror
                            <span class="form-text text-muted">Tipe foto yang diperbolehkan: png, jpg, jpeg.</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Nama Lengkap</label>
                        <div class="col-lg-9 col-xl-6">
                            <input name="name" type="text" class="form-control form-control-lg form-control-solid @error('name') is-invalid @enderror" value="{{Auth::user()->name}}" />
                            @error('name')
                                <span class="form-text text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Alamat Email</label>
                        <div class="col-lg-9 col-xl-6">
                            <input class="form-control form-control-lg form-control-solid" type="text" disabled value="{{Auth::user()->email}}" />
                            <span class="form-text text-muted">Email tidak bisa diubah</span>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-xl-3"></label>
                        <div class="col-lg-9 col-xl-6">
                            <h5 class="font-weight-bold mt-10 mb-6">Info Kontak</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">No Telepon</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg input-group-solid">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-phone"></i>
                                    </span>
                                </div>
                                <input type="text" name="telepon" class="form-control form-control-lg form-control-solid" value="{{Auth::user()->telepon}}" placeholder="No Telepon" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Alamat</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg input-group-solid">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-home"></i>
                                    </span>
                                </div>
                                <input type="text" name="address" class="form-control form-control-lg form-control-solid" value="{{Auth::user()->address}}" placeholder="Alamat" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-xl-3"></label>
                        <div class="col-lg-9 col-xl-6">
                            <h5 class="font-weight-bold mt-10 mb-6">Sosial Media</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Instagram</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg input-group-solid">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-instagram"></i>
                                    </span>
                                </div>
                                <input type="text" name="instagram" class="form-control form-control-lg form-control-solid" value="{{Auth::user()->instagram}}" placeholder="Url Instagram" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Facebook</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg input-group-solid">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-facebook"></i>
                                    </span>
                                </div>
                                <input type="text" name="facebook" class="form-control form-control-lg form-control-solid" value="{{Auth::user()->facebook}}" placeholder="Url Facebook" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Twitter</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg input-group-solid">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-twitter"></i>
                                    </span>
                                </div>
                                <input type="text" name="twitter" class="form-control form-control-lg form-control-solid" value="{{Auth::user()->twitter}}" placeholder="Url Twitter" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Youtube</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg input-group-solid">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-youtube"></i>
                                    </span>
                                </div>
                                <input type="text" name="youtube" class="form-control form-control-lg form-control-solid" value="{{Auth::user()->youtube}}" placeholder="Url Youtube" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('backend/assets/css/dropify.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('backend/assets/js/dropify.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/dropify.js') }}"></script>
@endpush