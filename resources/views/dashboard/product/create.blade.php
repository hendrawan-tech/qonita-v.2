@extends('dashboard.templates.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap py-5">
                @include('dashboard.templates.heading-card')
            </div>
            <div class="card-body">
                <form action="/dashboard/products/product" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nama Produk<span class="text-danger">*</span></label>
                                <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama Produk" value="{{old('name')}}">
                            </div>
                            @error('name')
                                <span class="text-danger ml-3">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Harga Produk<span class="text-danger">*</span></label>
                                <input name="price" type="text" class="form-control @error('price') is-invalid @enderror" placeholder="Harga Produk" value="{{old('price')}}">
                            </div>
                            @error('price')
                                <span class="text-danger ml-3">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Kategori Produk<span class="text-danger">*</span></label>
                                <select class="form-control select2 @error('category_product_id') is-invalid @enderror" id="kt_select2_1" name="category_id">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($category as $category)
                                        <option value="{{$category->id}}" {{old('category_id') == $category->id ? 'selected' : ''}}>
                                            {{$category->category}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @error('category_id')
                                <span class="text-danger ml-3">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Status Produk<span class="text-danger">*</span></label>
                                <select class="form-control @error('status') is-invalid @enderror" name="status">
                                    <option value="">Pilih Status</option>
                                    <option value="1" {{old('status') == 1 ? 'selected' : ''}}>Stock Ready</option>
                                    <option value="2" {{old('status') == 2 ? 'selected' : ''}}>Stock Not Ready</option>
                                </select>
                            </div>
                            @error('status')
                                <span class="text-danger ml-3">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Ukuran Produk<span class="text-danger">*</span></label>
                                <select class="form-control selectpicker @error('size') is-invalid @enderror" multiple name="size[]">
                                    @foreach ($size as $size)
                                        <option value="{{$size->id}}" {{old('size') == $size->id ? 'selected' : ''}}>
                                            {{$size->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @error('size')
                                <span class="text-danger ml-3">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Warna Produk<span class="text-danger">*</span></label>
                                <select class="form-control selectpicker" multiple name="color[]">
                                    @foreach ($color as $m)
                                        <option value="{{$m->id}}" {{old('color') == $m->id ? 'selected' : ''}}>{{$m->name}}</option>
                                    @endforeach
                                </select>
                                @error('color')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 mb-3">
                            <div class="form-group">
                                <label for="" class="control-label">Gambar Produk<span class="text-danger">*</span></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" name="photo[]" id="image" multiple class="custom-file-input" required>
                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label> 
                                    </div>
                                </div>
                                @error('photo')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                                <div class="image-preview-div bg-light"> </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Deskripsi Postingan <span class="text-danger">*</span></label>
                                <textarea class="summernote" id="kt_summernote_1" name="description">{{old('description')}}</textarea>
                                @error('description')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 text-right mt-3 mr-3">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('backend/assets/css/dropify.min.css') }}">
    <style>
        .image-preview-div img {
            padding: 10px;
            max-width: 200px;
        }
    </style>
@endpush

@push('scripts')
  <script src="{{asset('backend/assets/js/pages/crud/forms/editors/summernote.js')}}"></script>
  <script src="{{ asset('backend/assets/js/dropify.min.js') }}"></script>
  <script src="{{ asset('backend/assets/js/dropify.js') }}"></script>
  <script src="{{ asset('backend/assets/js/pages/crud/forms/widgets/select2.js') }}"></script>
  <script>
    $(function() {
        var previewImages = function(input, imgPreviewPlaceholder) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };
        $('#image').on('change', function() {
            previewImages(this, 'div.image-preview-div');
        });
    });
    $('#image').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
</script>
@endpush