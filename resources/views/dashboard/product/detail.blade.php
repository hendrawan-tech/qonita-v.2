@extends('dashboard.templates.master')
@push('styles')
<style>
    .img {
        position: relative !important;
        height: 450px; 
        width: 100%;
        overflow: hidden;
    }
    .img img, .img2 img {
        width: 100%; 
        height: 100%; 
        object-fit: cover;
    }
    .img2 {
        position: relative !important;
        height: 150px; 
        width: 100%;
        overflow: hidden;
    }
    .thumb {
        opacity: .3;
        transition: 0.3s;
    }

    .thumb:hover {
        opacity: 1;
        cursor: pointer;
    }

    @keyframes fadeIn {
        to {
            opacity: 1;
        }
    }

    .fade {
        opacity: 1;
        animation: fadeIn 0.5s forwards;
    }

    .thumb.active {
        opacity: 1;
    }
</style>
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Produk {{$item->name}}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 container-img">
                        <div class="img mb-3">
                            <img src="{{asset($item->image)}}" class="jumbo" alt="">
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="img2 mb-3">
                                    <img src="{{asset($item->image)}}" class="thumb active" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="img2 mb-3">
                                    <img src="{{asset($item->image2)}}" class="thumb" alt="">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="img2 mb-3">
                                    <img src="{{asset($item->image3)}}" class="thumb" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <h2>{{$item->name}}</h2>
                        <h2 class="mt-1">{{Helper::price($item->price)}}</h2>
                        <hr class="border-primary mt-2">
                        <div class="row">
                            <div class="col-lg-2">
                                <h4>Status : </h4>
                            </div>
                            <div class="col-lg-6">
                                <div>
                                    @if ($item->status == 1)
                                        <span class="badge badge-success">Stock Ready</span>
                                    @else
                                        <span class="badge badge-danger">Stock Not Ready</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <h4>Kategori : </h4>
                            </div>
                            <div class="col-lg-6">
                                <p>{{$item->category->category}}</p>
                            </div>
                        </div>
                        <hr class="border-primary mt-1">
                        <h4>Deskripsi : </h4>
                        <div class="font-italic font-sm mt--3">{!! $item->description !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    const container = document.querySelector('.container-img');
    const jumbo = document.querySelector('.jumbo');
    const thumbs = document.querySelectorAll('.thumb');
    container.addEventListener('click', function(e) {
        if(e.target.className == 'thumb') {
            jumbo.src = e.target.src;
            jumbo.classList.add('fade');
            setTimeout(function() {
                jumbo.classList.remove('fade');
            }, 100);

            thumbs.forEach(function(thumb) {
                thumb.className = 'thumb';
            });

            e.target.classList.add('active');
        }
    });
</script>
@endpush