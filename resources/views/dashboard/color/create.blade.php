@extends('dashboard.templates.master')
@section('content')

<form action="/dashboard/products/color" enctype="multipart/form-data" method="POST">
    @csrf
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-custom">
                <div class="card-header flex-wrap py-5">
                    @include('dashboard.templates.heading-card')
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nama Warna <span class="text-danger">*</span></label>
                                <input type="text" name="name" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" placeholder="Masukkan Nama Warna">
                                @error('name')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Pilih Warna <span class="text-danger">*</span></label>
                                <input type="color" name="color" value="{{old('color')}}" class="form-control @error('color') is-invalid @enderror" placeholder="">
                                @error('color')
                                    <span class="form-text text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <button class="btn btn-primary" type="submit">Tambah</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection