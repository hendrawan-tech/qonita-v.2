@extends('dashboard.templates.master')

@section('content')

<div class="row">
    <div class="col-lg-4">
        <div class="card card-custom">
            <div class="card-header flex-wrap py-5">
                @include('dashboard.templates.heading-card')
            </div>
            <div class="card-body">
                <form action="/dashboard/products/category" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <input type="text" name="category" value="{{old('category')}}" class="form-control @error('category') is-invalid @enderror" placeholder="Category">
                            @error('category')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 d-flex justify-content-between">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection