@extends('dashboard.templates.master')

@section('content')

@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<div class="row">
  <div class="col-lg-12">
    <div class="card card-custom">
      <div class="card-header flex-wrap py-5">
        @include('dashboard.templates.heading-card')
      </div>
      <div class="card-body" style="overflow: auto !important;">
        <table class="table table-separate table-head-custom" id="kt_datatable">
          <thead>
            <tr>
              <th>#</th>
              <th>Kode</th>
              <th>Nama Pembeli</th>
              <th>Email Pembeli</th>
              <th>Tanggal Order</th>
              <th>Status</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($order as $order)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>
                  <a href="{{Helper::permission()->url . '/' . $order->id}}" data-toggle="tooltip" data-placement="top" title="Detail">
                    {{$order->code}}
                  </a>
                </td>
                <td>{{$order->username}}</td>
                <td>{{$order->email}}</td>
                <td>{{$order->created_at}}</td>
                <td>
                  @if ($order->status == 1)
                    <span class="badge badge-danger">Pending</span>
                  @elseif($order->status == 2)
                    <span class="badge badge-warning">Proses</span>
                  @elseif($order->status == 3)
                    <span class="badge badge-success">Terkirim</span>
                  @else
                    <span class="badge badge-danger">Batal</span>
                  @endif
                </td>
                <td class="d-flex justify-content-center">
                  @if (Helper::permission()->edit == 1)
                    <a onclick='modal_status("{{ Helper::permission()->url . "/" . $order->id }}")' href="#" class="btn btn-sm btn-clean btn-hover-primary btn-icon mr-2" title="Edit details"><span class="svg-icon svg-icon-md"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "></path><rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"></rect></g></svg></span></a>
                  @endif
                  @if (Helper::permission()->delete)
                    @php
                      $linkdelete = Helper::permission()->url . '/' . $order->id
                    @endphp
                    <a href="#" onclick='modal_konfir("{{ $linkdelete }}")' class="btn btn-sm btn-clean btn-hover-danger btn-icon" title="Delete"><span class="svg-icon svg-icon-md"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path><path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path></g></svg></span></a>
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="status" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content modal-lg">
          <div class="modal-header">
              <h5 class="modal-title">Ubah Status</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&#xD7;</span></button>
          </div>
          <form action="" method="POST" name="fromstatus">
              @csrf
              @method('put')
              <div class="modal-body">
                  <div class="form-group">
                      <select class="custom-select" name="status" required>
                          <option value="">Pilih Status</option>
                          <option value="1">Pending</option>
                          <option value="2">Proses</option>
                          <option value="3">Terkirim</option>
                          <option value="0">Batal</option>
                      </select>
                  </div>
              </div>
              
              <div class="modal-footer">
                  <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
                  <button class="btn btn-success" type="submit">Ubah</button>
              </div>
          </form>
      </div>
  </div>
</div>
@endsection

@push('scripts')
  <script src="{{asset('assets/js/plugin/datatables/datatables.min.js')}}"></script>
  <script >
		$(document).ready(function() {
			$('#basic-datatables').DataTable({
			});

			$('#multi-filter-select').DataTable( {
				"pageLength": 5,
				initComplete: function () {
					this.api().columns().every( function () {
						var column = this;
						var select = $('<select class="form-control"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
								);

							column
							.search( val ? '^'+val+'$' : '', true, false )
							.draw();
						} );

						column.data().unique().sort().each( function ( d, j ) {
							select.append( '<option value="'+d+'">'+d+'</option>' )
						} );
					} );
				}
			});
		});

    function modal_status(url) {
      $('#status').modal('show', {
        backdrop: 'static'
      });
      document.fromstatus.action = url;
    }
	</script>
@endpush