@extends('dashboard.templates.master')
@section('content')
<div class="card card-custom overflow-hidden">
    <div class="card-body p-0">
        <div class="row justify-content-center py-8 px-8 py-md-27 px-md-0" style="background-image: linear-gradient(135deg, #0061f2 0%, rgba(105, 0, 199, 1) 100%);">
            <div class="col-md-9">
                <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                    <h1 class="display-4 text-white font-weight-boldest mb-10">INVOICE</h1>
                    <div class="d-flex flex-column align-items-md-end px-0">
                        <a href="#" class="mb-5" style="filter: brightness(2)">
                            <img src="{{asset(Helper::general()->logo)}}" alt="" />
                        </a>
                    </div>
                </div>
                <div class="border-bottom w-100 opacity-20"></div>
                <div class="d-flex justify-content-between text-white pt-6">
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolde mb-2r">DATA</span>
                        <span class="opacity-70">{{Helper::waktu($order->created_at)}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">INVOICE NO.</span>
                        <span class="opacity-70">{{$order->code}}</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">INVOICE TO.</span>
                        <span class="opacity-70">Nama : {{$order->username}}</span>
                        <span class="opacity-70">Email : {{$order->email}}</span>
                        <span class="opacity-70">No Telepon : {{$order->phone}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-9">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="pl-0 font-weight-bold text-muted text-uppercase">Item</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Harga</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Kuantitas</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Warna</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Ukuran</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Atas Nama</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order->orderItems as $item)
                            <tr class="font-weight-boldest font-size-lg">
                                <td class="pl-0 pt-7">{{$item->product->name}}</td>
                                <td class="text-right pt-7">{{Helper::price($item->product->price)}}</td>
                                <td class="text-right pt-7">{{$item->quantity}}</td>
                                <td class="text-right pt-7">{{$item->color->name}}</td>
                                <td class="text-right pt-7">{{$item->size->name}} ({{$item->size->size}})</td>
                                <td class="text-right pt-7">{{$item->behalf_of}}</td>
                                <td class="text-danger pr-0 pt-7 text-right">{{Helper::price($item->product->price * $item->quantity)}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice body-->
        <!-- begin: Invoice footer-->
        <div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-9">
                <div class="d-flex justify-content-between flex-column flex-md-row font-size-lg">
                    <div class="d-flex flex-column mb-10 mb-md-0">
                        <div class="font-weight-bolder font-size-lg mb-3">BANK TRANSFER</div>
                        <div class="d-flex justify-content-between mb-3">
                            <span class="mr-15 font-weight-bold">Atas Nama :</span>
                            <span class="text-right">{{$order->payment->name}}</span>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <span class="mr-15 font-weight-bold">No Rekening :</span>
                            <span class="text-right">{{$order->payment->card_number}}</span>
                        </div>
                        <div class="d-flex justify-content-between">
                            <span class="mr-15 font-weight-bold">Nama Bank :</span>
                            <span class="text-right">{{$order->payment->bank}}</span>
                        </div>
                    </div>
                    <div class="d-flex flex-column text-md-right">
                        <span class="font-size-lg font-weight-bolder mb-1">TOTAL HARGA</span>
                        <span class="font-size-h2 font-weight-boldest text-danger mb-1">{{Helper::price($order->total)}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Invoice footer-->
        <!-- begin: Invoice action-->
        <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
            <div class="col-md-9">
                <div class="d-flex justify-content-between">
                    <button type="button" class="btn btn-light-primary font-weight-bold" onclick="window.print();">Download Invoice</button>
                    <button type="button" class="btn btn-primary font-weight-bold" onclick="window.print();">Print Invoice</button>
                </div>
            </div>
        </div>
        <!-- end: Invoice action-->
        <!-- end: Invoice-->
    </div>
</div>
@endsection