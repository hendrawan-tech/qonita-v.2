@extends('layouts.app')
@section('content')
    @include('layouts.nav')
    <section class="katalog mt-5">
        <div class="container">
            <div class="row">
                <div class="col-auto mr-auto">
                    <b>Aksesoris Wanita</b>
                </div>
                <div class="col-auto">
                    <p>Urutkan <i class="fa fa-list"></i></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="card border-0 shadow-sm mb-2">
                        <div class="card-body">
                            <b>Brand</b>
                            <form action="" class="mt-2">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" checked id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Nibra's</label>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Rabbani</label>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">HaiHai</label>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Kencana Ungu</label>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card border-0 shadow-sm mb-2">
                        <div class="card-body">
                            <b>Harga</b>
                            <form action="" class="mt-2">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" checked id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Dibawah 1 Jt</label>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">1 Jt - 5 Jt</label>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">6 Jt - 10</label>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card border-0 shadow-sm mb-2">
                        <div class="card-body">
                            <b>Model</b>
                            <form action="" class="mt-2">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" checked id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Batuk</label>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Elegant</label>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Luxury</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    <div class="row">
                        @foreach ($products as $product)
                            <div class="col-lg-4 mb-4">
                                <div class="card border-0 shadow-sm item">
                                    <div class="img">
                                        <img src="{{ asset($product->photo[0]->photo) }}" class="card-img-top" alt="...">
                                        <span>New</span>
                                    </div>
                                    <div class="card-body">
                                        <div class="card-text">
                                            <p><a href="/product/{{ $product->slug }}" class="link">Nibra's</a> <br> <b
                                                    class="color-primary">{{ $product->name }}</b></p>
                                        </div>
                                        <div class="row">
                                            <div class="col-auto mr-auto">
                                                <i class="fa fa-heart"></i>
                                            </div>
                                            <div class="col-auto">
                                                <p>{{ Helper::price($product->price) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        {{ $products->links() }}
                    </div>
                </div>

                @foreach ($products as $item)
                    <div class="col-md-3 mt-4">
                        <div class="card collection-card position-relative overflow-hidden"
                            style="background: url({{ asset($item->photo[0]->photo) }});">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
