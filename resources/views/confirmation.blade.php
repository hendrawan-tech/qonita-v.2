@extends('layouts.app')
@section('content')
<section class="header overflow-hidden position-relative">
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="img/logo.png" alt=""> <span
                    class="color-primary nav-title">Qonita</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/')}}">Home</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#">Lifestyle</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#">Fashion Trend</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link color-primary" href="{{ url('/catalog')}}">Katalog</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">

                    <button class="btn btn-primary btn-banner"><i class="fas fa-search"></i> Search</button>
                </form>
            </div>
        </div>
    </nav>
</section>

<div class="jumbotron jumbotron-fluid cart bg-light mb-0">
    <div class="container">
        <div class="row justify-content-between text-center mt-5">
            <div class="col-lg-3 col-md-3 text-left">
                <p><i class="fa fa-arrow-left"></i></p>
            </div>
            <div class="col-lg-6 text-center">
                <h3>Detail Pemesanan</h3>
            </div>
            <div class="col-lg-3 col-md-3 text-right">
                <p>[{{$result->code}}]</p>
            </div>
        </div>

        <div class="row mt-5 mb-5">
            <div class="col-lg-6">
                <div class="card mt-3">
                    <div class="card-body">
                        <p class="text-center font-weight-bold text-uppercase">Identitas Pembeli</p>
                        <table class="table justify-content-between small">
                            <tbody>
                                <tr>
                                    <td>Nama Pembeli</td>
                                    <td class="text-right">{{$result->username}}</td>
                                </tr>
                                <tr>
                                    <td>Email Pembeli</td>
                                    <td class="text-right">{{$result->email}}</td>
                                </tr>
                                <tr>
                                    <td>No. Handphone</td>
                                    <td class="text-right">{{$result->phone}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-body">
                        <p class="text-center font-weight-bold text-uppercase">Metode Pembayaran</p>
                        <table class="table justify-content-between small">
                            <tbody>
                                <tr>
                                    <td>Nama Bank</td>
                                    <td class="text-right">{{$result->payment->bank}}</td>
                                </tr>
                                <tr>
                                    <td>No. Rekening</td>
                                    <td class="text-right">{{$result->payment->card_number}}</td>
                                </tr>
                                <tr>
                                    <td>Atas Nama</td>
                                    <td class="text-right">{{$result->payment->name}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card mt-3">
                    <div class="card-body">
                        <p class="font-weight-bold text-uppercase">Rincian Transaksi Anda</p>
                        <div class="row small">
                            <div class="col-lg-6">
                                <p>tanggal Pembelian <span class="text-primary">{{ date("d-m-Y") }}</span></p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-right">Bayar Sebelum <span class="text-danger">{{ date("d-m-Y", time() + 86400) }}</span></p>
                            </div>
                        </div>
                        <table class="table justify-content-between small">
                            <thead style="border: none !important;">
                                <tr>
                                    <th scope="col">Item</th>
                                    <th scope="col">Warna</th>
                                    <th scope="col">Ukuran</th>
                                    <th scope="col">Kuantitas</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Atas Nama</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($result->orderItems as $product)
                                <tr>
                                    <td>{{$product->product->name}}</td>
                                    <td>{{$product->color->name}}</td>
                                    <td>{{$product->size->size}}</td>
                                    <td>x{{$product->quantity}}</td>
                                    <td>{{Helper::price($product->product->price)}}</td>
                                    <td>{{$product->behalf_of}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td>Kode Unik</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{"Rp " . number_format($unique,0,',','.')}}</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="font-weight-bold">{{"Rp " . number_format($result->total,0,',','.')}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="https://api.whatsapp.com/send?phone=6281311000300" class="btn btn-primary btn-2 mt-2 button w-100">Kirim Bukti Transaksi</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection