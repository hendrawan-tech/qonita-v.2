@extends('layouts.app')
@section('content')
@include('layouts.nav')
<section class="katalog mt-5">
    <div class="container">
        <form action="/checkout" method="POST">
            @csrf
            @if (Cookie::get('product') == null)
                <div class="row justify-content-between mt-5">
                    <div class="col-lg-12">
                        <div class="card" style="overflow-x: scroll !important;">
                            <div class="card-body">
                                <table class="table">
                                    <thead style="border: none !important;">
                                        <tr>
                                            <th scope="col">Foto</th>
                                            <th scope="col">Produk</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Atas Nama</th>
                                            <th scope="col">Opsi</th>
                                        </tr>
                                    </thead>
                                </table>
                                <h5 class="text-center text-danger">Anda belum menambahkan item ke keranjang</h5>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row justify-content-between mt-5">
                    <div class="col-lg-12">
                        <div class="card" style="overflow-x: scroll !important;">
                            <div class="card-body">
                                <table class="table">
                                    <thead style="border: none !important;">
                                        <tr>
                                            <th scope="col">Foto</th>
                                            <th scope="col">Produk</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Atas Nama</th>
                                            <th scope="col">Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tmpdata">
        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
    
            <div class="row mt-5 mb-5">
                <div class="col-lg-6">
                    <div class="card mt-3">
                        <div class="card-body">
                            <p class="text-center font-weight-bold">LENGKAPI DATA DIRI ANDA</p>
                            <form action="">
                                <div class="form-group">
                                    <input type="text" name="username" class="form-control" placeholder="Nama Lengkap">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Alamat Email">
                                </div>
                                <div class="form-group">
                                    <input type="telp" name="phone" class="form-control" placeholder="No Telepon">
                                </div>
                                <div class="input-group mb-3">
                                    <select class="custom-select" name="payment_id">
                                        <option selected>Metode Pembayaran</option>
                                        @foreach ($payments as $payment)
                                            <option value="{{$payment->id}}">{{$payment->bank}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card mt-3">
                        <div class="card-body">
                            <p class="text-center font-weight-bold text-uppercase">Rincian Transaksi Anda</p>
                            <table class="table justify-content-between">
                                <thead style="border: none !important;">
                                    <tr>
                                        <th scope="col">Item</th>
                                        <th scope="col">Jumlah</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody id="rincian">
                                    
                                </tbody>
                            </table>

                            <div class="row">
                                <button type="submit" class="btn btn-primary ml-3 mr-3 mt-2" style="width: 100%; height: 50px;">
                                    Proses Pembayaran
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<script>
    window.addEventListener("load", function(event){
        loadData();
    })
    // console.log("okok");
    var data = <?php echo json_encode($data); ?>;
    //  data = JSON.parse(data);
    function loadData() {
        var html='';
        var rincian = '';
        var total=0;
        data.forEach((element, index) => {
            html+="<tr><td><div style='width:50px;height:50px'><img style='width:100%;height:100%;object-fit:cover;' src="+element.photo+"></div></td><td>"+element.name+"</td>";
            html+="<td>"+formatRupiah(element.price.toString())+"</td>";
            html+="<td><div class='def-number-input number-input safari_only'><button onclick='minus("+ index +")' class='minus' type='button'></button><input class='quantity' min='1' name='quantity[]' value='"+ element.quantity +"' type='number'>";
            html+="<button onclick='tambah("+ index +")' class='plus' type='button'></button></div></td>";
            html+="<td><input type='text' class='form-control' name='behalf_of[]' required placeholder='Nama Pembeli'></td>";
            html+="<td><a href='/cart?order_id="+index+"' class='text-danger' style='font-size: 20px !important;'><i class='fa fa-trash'></i></a></tr>";
            html+="<input type='hidden' name='size_id[]' value='"+element.size_id+"'>";
            html+="<input type='hidden' name='color_id[]' value='"+element.color_id+"'>";
            total += element.total_price;

            rincian+= "<tr><td>"+ element.name +"</td><td>x"+ element.quantity +"</td>" +
                      "<td>" + formatRupiah(element.total_price.toString()) +"</td></tr>";
        });
        rincian+= "<tr><td>Total</td><td></td><td class='text-primary font-weight-bold'>"+ formatRupiah(total.toString()) +"</td></tr>";
        document.getElementById("tmpdata").innerHTML = html;
        document.getElementById("rincian").innerHTML = rincian;
        // console.log("gg",data);
    }

    function formatRupiah(angka){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return "Rp. " + rupiah;
    }
    
    function tambah(urutan) {
        var tmp = [];
        data.forEach((element, index) => {
            if(urutan == index) {
                element['quantity'] = parseInt(element.quantity) + 1;
                element['total_price'] = parseInt(element.quantity) * element.price;
                tmp.push(element);
            } else {
                tmp.push(element)
            }
        });
        data = tmp;
        loadData();
        // postcart();
    }

    function minus(urutan) {
        if((parseInt(data[urutan]['quantity']) - 1) >= 1){
            var tmp = [];
            data.forEach((element, index) => {
                if(urutan == index) {
                    element['quantity'] = parseInt(element.quantity) - 1;
                    element['total_price'] = parseInt(element.quantity) * element.price;
                    tmp.push(element);
                } else {
                    tmp.push(element)
                }
            });
            loadData();
        }
    }
    // function hapus(urutan) {
    //     data.splice(urutan, 1);
    //     loadData();
    // }
</script>
@endsection