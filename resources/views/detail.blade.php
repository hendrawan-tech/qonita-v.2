@extends('layouts.app')
@section('content')
    @include('layouts.nav')
    <section class="detail bg-light">
        <div class="container">
            <div class="row justify-content-between mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 container-img">
                            <div class="img mb-3">
                                <img src="{{ asset($product->photo[0]->photo) }}" class="jumbo" alt="">
                            </div>
                            <div class="row">
                                @for ($i = 0; $i < count($product->photo); $i++)
                                    @if ($i == 0)
                                        <div class="col-4">
                                            <div class="img2 mb-3">
                                                <img src="{{ asset($product->photo[$i]->photo) }}" class="thumb active"
                                                    alt="">
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-4">
                                            <div class="img2 mb-3">
                                                <img src="{{ asset($product->photo[$i]->photo) }}" class="thumb" alt="">
                                            </div>
                                        </div>
                                    @endif
                                @endfor
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <h4 style="font-weight: bold;">{{ $product->name }}</h4>
                            <div class="stock color-primary mt-2 mb-2">
                                @if ($product->status == 1)
                                    <i class="fa fa-check-circle"></i> <span class="mr-3">Stock Ready</span>
                                @else
                                    <i class="fa fa-times-circle"></i> <span class="mr-3">Stock Not Ready</span>
                                @endif
                                <i class="fa fa-share-alt-square"></i> <span>Bagikan</span>
                            </div>
                            <h2 class="mb-2 mt-1" id="total">{{ Helper::price($product->price) }}</h2>
                            <hr class="border-primary">
                            <p><b>Panduan Pembeli</b></p>
                            <form action="/cart" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label for="">Warna</label>
                                        <select class="custom-select" required name="color_id">
                                            @foreach ($product->color as $item)
                                                <option value="{{ $item->color->id }}">{{ $item->color->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="">Ukuran</label>
                                        <select class="custom-select" required name="size_id">
                                            @foreach ($product->size as $item)
                                                <option value="{{ $item->size->id }}">{{ $item->size->name }}
                                                    ({{ $item->size->size }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Jumlah : </label>
                                        <div class="def-number-input number-input safari_only">
                                            <button onclick="Minus()" type="button" class="minus"></button>
                                            <input id="quantity" class="quantity" min="1" name="quantity" value="1"
                                                type="number">
                                            <button onclick="Tambah()" type="button" class="plus"></button>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <input type="hidden" name="name" value="{{ $product->name }}">
                                <input type="hidden" name="price" value="{{ $product->price }}">
                                <input type="hidden" name="photo" value="{{ $product->photo[0]->photo }}">

                                <div class="row mt-4">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-4 mt-2"
                                            style="width: 100%; height: 50px;">
                                            <i class="fa fa-shopping-cart"></i> Tambah ke Keranjang
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <hr class="border-primary mt-4">
                            <div class="font-italic font-sm mt-4">{!! $product->description !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="katalog mt-5">
        <div class="container">
            <div class="row">
                <div class="col-auto mr-auto">
                    <b class="color-primary">Rekomendasi</b>
                    <hr class="border-primary">
                </div>
                <div class="col-auto">
                    <a href="" class="color-primary">View More</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        @foreach ($recomended as $item)
                            <div class="col-lg-3 mb-4">
                                <div class="card border-0 shadow-sm item">
                                    <div class="img">
                                        <img src="{{ asset($item->photo[0]->photo) }}" class="card-img-top"
                                            alt="{{ $item->name }}">
                                        <span>New</span>
                                    </div>
                                    <div class="card-body">
                                        <div class="card-text">
                                            <p>{{ $item->category->category }}<br> <b class="color-primary">
                                                    <a class="color-primary"
                                                        href="/product/{{ $item->slug }}">{{ $item->name }}</a>
                                                </b></p>
                                        </div>
                                        <div class="row">
                                            <div class="col-auto mr-auto">
                                                <i class="fa fa-heart"></i>
                                            </div>
                                            <div class="col-auto">
                                                <p>{{ Helper::price($item->price) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        const container = document.querySelector('.container-img');
        const jumbo = document.querySelector('.jumbo');
        const thumbs = document.querySelectorAll('.thumb');
        container.addEventListener('click', function(e) {
            if (e.target.className == 'thumb') {
                jumbo.src = e.target.src;
                jumbo.classList.add('fade');
                setTimeout(function() {
                    jumbo.classList.remove('fade');
                }, 100);

                thumbs.forEach(function(thumb) {
                    thumb.className = 'thumb';
                });

                e.target.classList.add('active');
            }
        });

    </script>

    <script>
        var harga = < ? = $product - > price ?>;    var quantity = 1;

        function Tambah() {
            quantity++;
            var total = quantity * harga;
            document.getElementById('total').innerHTML = formatRupiah(total.toString());
            document.getElementById('quantity').value = quantity;
        }

        function Minus() {
            if (quantity > 1) {
                quantity--;
                var total = quantity * harga;
                document.getElementById('total').innerHTML = formatRupiah(total.toString());
                document.getElementById('quantity').value = quantity;
            }
        }

        function formatRupiah(angka) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return "Rp. " + rupiah;
        }

    </script>
@endpush
