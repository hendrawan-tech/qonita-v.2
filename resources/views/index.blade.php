@extends('layouts.app')

@section('content')
<section class="header overflow-hidden position-relative">
    <nav class="navbar navbar-expand-lg navbar-light" style="z-index: 99;">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="{{asset('assets/qonita/img/logo.png')}}" alt=""> <span class="color-primary nav-title">Qonita</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link color-primary" href="{{ url('/')}}">Home</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#">Lifestyle</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#">Fashion Trend</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('catalog')}}">Katalog</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <div class="cart mr-4 text-primary">
                        <a href="/cart" class="text-primary"><i class="fa fa-store"></i></a>
                    </div>
                    <button class="btn btn-primary btn-banner"><i class="fas fa-search"></i> Search</button>
                </form>
            </div>
        </div>
    </nav>
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-6 color-primary banner-content">
                <h1 class="display-2 times">Fashion</h1>
                <h1 class="display-1 font-weight-bold">TREND</h1>
                <p class="lead text-uppercase min-p">Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry.</p>
                <p class="lead">
                    <a class="color-primary" href="#" role="button"><i class="fab fa-instagram" aria-hidden="true"></i>
                        @shophi-a</a>
                    <a class="color-primary" href="#" role="button"><i class="fab fa-amazon" aria-hidden="true"></i>
                        Shophi's</a>
                </p>
            </div>
            <div class="col-md-6 banner-image position-relative">
                <img src="{{asset('assets/qonita/img/banner.png')}}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
    <div class="circle position-absolute circle-top-right bg-pink"></div>
</section>

<section class="about overflow-hidden position-relative">
    <div class="row">
        <div class="col-md-6 image-none">
            <img src="{{asset('assets/qonita/img/about.png')}}" class="img-fluid" alt="">
        </div>
        <div class="col-md-6 margin-auto text-center">
            <div class="container-about">
                <h1 class="font-weight-bold">LifeStyle Fashion</h1>
                <hr>
                <button class="btn text-primary rounded-pill text-uppercase border border-primary btn-big-w"><i class="fas fa-search"></i> GO Store</button>
                <p class="text-uppercase min-p lead">All Trend with high quality product Branded International</p>
                <p class="lead">
                    <a class="color-primary" href="#" role="button"><i class="fab fa-instagram" aria-hidden="true"></i>
                        @shophi-a</a>
                    <a class="color-primary" href="#" role="button"><i class="fab fa-amazon" aria-hidden="true"></i>
                        Shophi's</a>
                </p>
            </div>
        </div>
    </div>
    <div class="circle position-absolute circle-bottom-left bg-orange"></div>
</section>
<div style="clear:both"></div>

<section class="collection overflow-hidden position-relative text-center bg-grey">
    <div class="container container-collection">
        <h1 class="text-uppercase color-primary">COLLECTION</h1>
        <hr>
        <div class="row">
            @foreach ($product as $product)
            <div class="col-md-4">
                <div class="card collection-card position-relative overflow-hidden" style="background: url({{asset($product->photo[0]->photo)}});">
                    <div class="card-image">
                        <span class="color-primary nav-title">Qonita</span>
                    </div>
                    <div class="card-body position-absolute">
                        <h4 class="text-uppercase text-white">{{$product->name}}</h4>
                        <hr class="bg-white">
                        <p class="text-uppercase text-white">{{$product->category->category}}</p>
                        <a href="/product/{{$product->slug}}" class="btn btn-default  rounded-pill text-uppercase border border-white text-white"><i class="fas fa-search"></i> Detail</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<div style="clear:both"></div>
<section class="overflow-hidden position-relative our-product">
    <div class="row">
        <div class="col-md-6 margin-auto text-center">
            <div class="container-about">
                <h1 class="font-weight-bold">Our Product</h1>
                <hr>
                <p class="text-primary text-uppercase active">Islamic Fashion</p>
                <p class="text-uppercase">Child fashion</p>
                <p class="text-uppercase">couple fashion</p>
                <p class="text-uppercase">traditional fashion</p>
                <hr>
                <button class="btn text-primary rounded-pill text-uppercase border border-primary btn-big-w"> <i class="fas fa-tshirt"></i> Catalog</button>
            </div>
        </div>
        <div class="col-md-6 image-none">
            <img src="{{asset('assets/qonita/img/our.png')}}" class="img-fluid" alt="">
        </div>
        <div class="circle position-absolute circle-bottom-right bg-purple"></div>
    </div>
    <!-- <div class="circle position-absolute circle-bottom-left bg-purple"></div> -->
</section>
<section class="product bg-grey mb-4">
    <div class="container">
        <h1 class="text-uppercase text-center color-primary">@Qonita</h1>
        <hr>
        <div class="row no-gutters">
            @foreach ($gallery as $gallery)
            <div class="col-md-3">
                <div class="card collection-card position-relative overflow-hidden" style="background: url({{asset($gallery->photo)}});">
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<section class="brands">
    <h1 class="text-uppercase text-center color-primary">Brands</h1>
    <hr>
    <div class="container text-center">
        <div class="row">
            <div class="col-md-3">
                <img src="{{asset('assets/qonita/img/brand1.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3">
                <img src="{{asset('assets/qonita/img/brand2.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3">
                <img src="{{asset('assets/qonita/img/brand3.png')}}" class="img-fluid" alt="">
            </div>
            <div class="col-md-3">
                <img src="{{asset('assets/qonita/img/brand4.png')}}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>
@endsection