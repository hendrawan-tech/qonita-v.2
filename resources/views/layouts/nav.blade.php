<nav class="navbar navbar-expand-lg navbar-light" style="z-index: 99;">
    <div class="container">
        <a class="navbar-brand" href="#"><img src="{{asset(Helper::general()->logo)}}" alt=""> <span
                class="color-primary nav-title">Qonita</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link {{ Request::segment(1) === null ? 'active' : ''}}" href="/">Home</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="#">Lifestyle</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="#">Fashion Trend</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::segment(1) === 'catalog' ? 'active' : ''}}" href="/catalog">Katalog</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <div class="cart mr-4 text-primary">
                    <a href="/cart" class="text-primary"><i class="fa fa-store"></i></a>
                </div>
                <button class="btn btn-primary btn-banner"><i class="fas fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</nav>