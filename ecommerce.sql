-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 11, 2021 at 10:57 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_menus`
--

CREATE TABLE `access_menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `access_menus`
--

INSERT INTO `access_menus` (`id`, `role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(2, 1, 2, '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(3, 1, 3, '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(4, 1, 4, '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(5, 1, 5, '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(6, 1, 6, '2021-01-10 08:04:15', '2021-01-10 08:04:15'),
(7, 1, 7, '2021-01-10 08:04:15', '2021-01-10 08:04:15'),
(8, 1, 8, '2021-01-10 08:04:15', '2021-01-10 08:04:15'),
(9, 2, 1, '2021-02-01 10:28:03', '2021-02-01 10:28:03'),
(12, 2, 4, '2021-02-01 10:28:03', '2021-02-01 10:28:03'),
(13, 2, 5, '2021-02-01 10:28:03', '2021-02-01 10:28:03'),
(14, 2, 6, '2021-02-01 10:28:03', '2021-02-01 10:28:03'),
(15, 2, 7, '2021-02-01 10:28:03', '2021-02-01 10:28:03'),
(16, 2, 8, '2021-02-01 10:28:03', '2021-02-01 10:28:03');

-- --------------------------------------------------------

--
-- Table structure for table `access_submenus`
--

CREATE TABLE `access_submenus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `submenu_id` bigint(20) UNSIGNED NOT NULL,
  `create` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `edit` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `delete` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `access_submenus`
--

INSERT INTO `access_submenus` (`id`, `role_id`, `submenu_id`, `create`, `edit`, `delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(2, 1, 2, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(3, 1, 3, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(4, 1, 4, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(5, 1, 5, '0', '0', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(6, 1, 6, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(7, 1, 7, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(8, 1, 8, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(9, 1, 9, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(10, 1, 10, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(11, 1, 11, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(12, 1, 12, '1', '1', '1', '2021-01-10 07:52:24', '2021-01-10 09:17:10'),
(13, 1, 13, '1', '1', '1', '2021-01-10 08:04:16', '2021-01-10 09:17:10'),
(14, 1, 14, '1', '1', '1', '2021-01-10 08:04:16', '2021-01-10 09:17:10'),
(15, 1, 15, '1', '1', '1', '2021-01-10 08:04:16', '2021-01-10 09:17:10'),
(16, 1, 16, '0', '1', '0', '2021-01-10 08:04:16', '2021-01-10 09:17:10'),
(17, 1, 17, '1', '1', '1', '2021-01-10 08:04:16', '2021-01-10 09:17:10'),
(18, 1, 18, '1', '1', '0', '2021-01-10 09:17:10', '2021-01-10 09:17:10'),
(19, 2, 1, '0', '0', '0', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(23, 2, 5, '0', '0', '1', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(27, 2, 9, '1', '1', '1', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(28, 2, 10, '0', '1', '1', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(29, 2, 11, '0', '1', '1', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(30, 2, 12, '0', '1', '1', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(31, 2, 13, '1', '1', '0', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(32, 2, 14, '1', '1', '0', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(33, 2, 15, '1', '1', '0', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(34, 2, 16, '0', '1', '0', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(35, 2, 17, '1', '1', '1', '2021-02-01 10:28:03', '2021-02-11 15:55:03'),
(36, 2, 18, '1', '1', '1', '2021-02-01 10:28:03', '2021-02-11 15:55:03');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_products`
--

CREATE TABLE `category_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_products`
--

INSERT INTO `category_products` (`id`, `category`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Kerudung', 'kerudung', '2021-01-10 09:23:05', '2021-01-10 09:23:05'),
(2, 'Baju', 'baju', '2021-01-10 09:23:44', '2021-01-10 09:23:44');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `color`, `created_at`, `updated_at`) VALUES
(1, 'Ungu', '#7300ff', '2021-01-10 08:58:02', '2021-01-10 08:58:02'),
(2, 'Biru', '#006eff', '2021-01-10 09:02:11', '2021-01-10 09:02:11'),
(3, 'Merah Muda', '#d400ff', '2021-01-10 09:02:27', '2021-01-10 09:02:27'),
(4, 'Hitam', '#000000', '2021-01-10 09:02:52', '2021-01-10 09:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `color_products`
--

CREATE TABLE `color_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `color_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `color_products`
--

INSERT INTO `color_products` (`id`, `color_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-01-10 09:59:53', '2021-01-10 09:59:53'),
(2, 2, 1, '2021-01-10 09:59:53', '2021-01-10 09:59:53');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `generals`
--

CREATE TABLE `generals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tagline` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_color` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondary_color` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gradient_primary` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gradient_secondary` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tw` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generals`
--

INSERT INTO `generals` (`id`, `logo`, `favicon`, `title`, `tagline`, `primary_color`, `secondary_color`, `gradient_primary`, `gradient_secondary`, `ig`, `fb`, `yt`, `tw`, `email`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'postImage/yhC6xGkg7OfZ6KMk.png', NULL, 'Qonita', 'Site Tagline', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-10 07:52:24', '2021-01-10 16:06:31');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `menu`, `icon`, `url`, `is_active`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', '<span class=\"svg-icon menu-icon\"><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http:/www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\">\r\n            <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\r\n                <polygon points=\"0 0 24 0 24 24 0 24\"/>\r\n                <path d=\"M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z\" fill=\"#000000\" fill-rule=\"nonzero\"/>\r\n                <path d=\"M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z\" fill=\"#000000\" opacity=\"0.3\"/>\r\n            </g>\r\n        </svg></span>', '/dashboard/homes', '1', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(2, 'Management Konten', '<span class=\"svg-icon menu-icon\"><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http:/www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\">\r\n            <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\r\n                <rect x=\"0\" y=\"0\" width=\"24\" height=\"24\"/>\r\n                <path d=\"M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z\" fill=\"#000000\" opacity=\"0.3\"/>\r\n                <path d=\"M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z\" fill=\"#000000\"/>\r\n            </g>\r\n        </svg></span>', '/dashboard/posts', '1', '3', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(3, 'Management', '<span class=\"svg-icon menu-icon\"><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http:/www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\">\r\n            <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\r\n                <rect x=\"0\" y=\"0\" width=\"24\" height=\"24\"/>\r\n                <rect fill=\"#000000\" x=\"4\" y=\"4\" width=\"7\" height=\"7\" rx=\"1.5\"/>\r\n                <path d=\"M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z\" fill=\"#000000\" opacity=\"0.3\"/>\r\n            </g>\r\n        </svg></span>', '/dashboard/managements', '1', '4', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(4, 'Pengguna', '<span class=\"svg-icon menu-icon\"><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http:/www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\">\r\n            <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\r\n                <polygon points=\"0 0 24 0 24 24 0 24\"/>\r\n                <path d=\"M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z\" fill=\"#000000\" fill-rule=\"nonzero\" opacity=\"0.3\"/>\r\n                <path d=\"M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z\" fill=\"#000000\" fill-rule=\"nonzero\"/>\r\n            </g>\r\n        </svg></span>', '/dashboard/users', '1', '7', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(5, 'Pengaturan', '<span class=\"svg-icon menu-icon\"><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http:/www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\"><g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><rect x=\"0\" y=\"0\" width=\"24\" height=\"24\"/><path d=\"M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z\" fill=\"#000000\"/><path d=\"M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z\" fill=\"#000000\" opacity=\"0.3\"/></g></svg></span>', '/dashboard/settings', '1', '8', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(6, 'Management Produk', '<span class=\"svg-icon menu-icon\"><!--begin::Svg Icon | path:C:\\wamp64\\www\\keenthemes\\themes\\metronic\\theme\\html\\demo1\\dist/../src/media/svg/icons\\Shopping\\Box2.svg--><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\">     <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">         <rect x=\"0\" y=\"0\" width=\"24\" height=\"24\"/>         <path d=\"M4,9.67471899 L10.880262,13.6470401 C10.9543486,13.689814 11.0320333,13.7207107 11.1111111,13.740321 L11.1111111,21.4444444 L4.49070127,17.526473 C4.18655139,17.3464765 4,17.0193034 4,16.6658832 L4,9.67471899 Z M20,9.56911707 L20,16.6658832 C20,17.0193034 19.8134486,17.3464765 19.5092987,17.526473 L12.8888889,21.4444444 L12.8888889,13.6728275 C12.9050191,13.6647696 12.9210067,13.6561758 12.9368301,13.6470401 L20,9.56911707 Z\" fill=\"#000000\"/>         <path d=\"M4.21611835,7.74669402 C4.30015839,7.64056877 4.40623188,7.55087574 4.5299008,7.48500698 L11.5299008,3.75665466 C11.8237589,3.60013944 12.1762411,3.60013944 12.4700992,3.75665466 L19.4700992,7.48500698 C19.5654307,7.53578262 19.6503066,7.60071528 19.7226939,7.67641889 L12.0479413,12.1074394 C11.9974761,12.1365754 11.9509488,12.1699127 11.9085461,12.2067543 C11.8661433,12.1699127 11.819616,12.1365754 11.7691509,12.1074394 L4.21611835,7.74669402 Z\" fill=\"#000000\" opacity=\"0.3\"/>     </g> </svg><!--end::Svg Icon--></span>', '/dashboard/products', '1', '2', '2021-01-10 07:55:35', '2021-01-10 07:57:00'),
(7, 'Pesanan', '<span class=\"svg-icon menu-icon\"><!--begin::Svg Icon | path:C:\\wamp64\\www\\keenthemes\\themes\\metronic\\theme\\html\\demo1\\dist/../src/media/svg/icons\\Shopping\\Cart1.svg--><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\">     <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">         <rect x=\"0\" y=\"0\" width=\"24\" height=\"24\"/>         <path d=\"M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z\" fill=\"#000000\" opacity=\"0.3\"/>         <path d=\"M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z\" fill=\"#000000\"/>     </g> </svg><!--end::Svg Icon--></span>', '/dashboard/orders', '1', '5', '2021-01-10 07:58:19', '2021-01-10 07:58:19'),
(8, 'Pembayaran', '<span class=\"svg-icon menu-icon\"><!--begin::Svg Icon | path:C:\\wamp64\\www\\keenthemes\\themes\\metronic\\theme\\html\\demo1\\dist/../src/media/svg/icons\\Shopping\\Credit-card.svg--><svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\">     <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">         <rect x=\"0\" y=\"0\" width=\"24\" height=\"24\"/>         <rect fill=\"#000000\" opacity=\"0.3\" x=\"2\" y=\"5\" width=\"20\" height=\"14\" rx=\"2\"/>         <rect fill=\"#000000\" x=\"2\" y=\"8\" width=\"20\" height=\"3\"/>         <rect fill=\"#000000\" opacity=\"0.3\" x=\"16\" y=\"14\" width=\"4\" height=\"2\" rx=\"1\"/>     </g> </svg><!--end::Svg Icon--></span>', '/dashboard/payments', '1', '6', '2021-01-10 07:59:27', '2021-01-10 07:59:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_07_15_083658_create_roles_table', 1),
(5, '2020_07_15_083709_create_menus_table', 1),
(6, '2020_07_15_083720_create_submenus_table', 1),
(7, '2020_07_15_083747_create_access_submenus_table', 1),
(8, '2020_07_15_085750_add_role_to_users_table', 1),
(9, '2020_07_25_212525_create_access_menus_table', 1),
(10, '2020_08_01_160126_create_categories_table', 1),
(11, '2020_08_01_160210_create_posts_table', 1),
(12, '2020_08_06_162434_create_generals_table', 1),
(13, '2020_11_26_173017_create_quotes_table', 1),
(14, '2020_12_04_223156_google_id_column', 1),
(15, '2020_12_13_080954_create_tags_table', 1),
(16, '2020_12_13_081042_create_post_tags_table', 1),
(17, '2020_12_13_081114_create_comments_table', 1),
(18, '2021_01_10_104525_create_category_products_table', 1),
(19, '2021_01_10_104555_create_products', 1),
(20, '2021_01_10_104742_create_payments_table', 1),
(21, '2021_01_10_104756_create_orders_table', 1),
(22, '2021_01_10_142201_create_photo_products_table', 1),
(23, '2021_01_10_142416_create_colors_table', 1),
(24, '2021_01_10_142449_create_sizes_table', 1),
(25, '2021_01_10_142516_create_color_products_table', 1),
(26, '2021_01_10_142538_create_size_products_table', 1),
(27, '2021_01_10_142607_add_column_to_products_table', 1),
(28, '2021_01_10_143056_add_column_to_orders_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `username`, `email`, `phone`, `total`, `code`, `payment_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Hendrawan', 'hendrawan@gmail.com', '083856324980', '100072', 'OR13255', 1, '1', '2021-01-10 16:32:49', '2021-01-10 16:32:49'),
(2, 'Sabyan', 'sabyan@gmail.com', '0823325211236', '50817', 'OR81359', 1, '1', '2021-01-10 16:36:59', '2021-01-10 16:36:59'),
(3, 'Testung', 'tes@gmail.com', '0823828378828', '50607', 'OR19374', 1, '1', '2021-01-10 16:55:01', '2021-01-10 16:55:01'),
(4, 'Testing', 'tess@gmail.com', '083287328287', '150066', 'OR30493', 1, '1', '2021-01-10 16:58:30', '2021-01-10 16:58:30');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `behalf_of` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `color_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `product_id`, `order_id`, `quantity`, `behalf_of`, `size_id`, `created_at`, `updated_at`, `color_id`) VALUES
(1, 1, 1, '1', 'Hendra', 1, '2021-01-10 16:32:49', '2021-01-10 16:32:49', 1),
(2, 1, 1, '1', 'Hendra', 2, '2021-01-10 16:32:49', '2021-01-10 16:32:49', 2),
(3, 1, 2, '2', 'Sabyan', 1, '2021-01-10 16:36:59', '2021-01-10 16:36:59', 1),
(4, 1, 3, '2', 'Seromben', 1, '2021-01-10 16:55:01', '2021-01-10 16:55:01', 1),
(5, 1, 4, '3', 'Tes Ho', 1, '2021-01-10 16:58:30', '2021-01-10 16:58:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `name`, `card_number`, `bank`, `created_at`, `updated_at`) VALUES
(1, 'Qonita', '3782349819289', 'Bank Central Asia (BCA)', '2021-01-10 10:08:51', '2021-01-10 10:08:51');

-- --------------------------------------------------------

--
-- Table structure for table `photo_products`
--

CREATE TABLE `photo_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photo_products`
--

INSERT INTO `photo_products` (`id`, `photo`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 'postImage/zjeky9LeOYjmr3Xo.jpeg', 1, '2021-01-10 09:59:53', '2021-01-10 09:59:53'),
(2, 'postImage/4ASpePzTphcEBTiI.jpeg', 1, '2021-01-10 09:59:53', '2021-01-10 09:59:53');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_tags`
--

CREATE TABLE `post_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `price`, `status`, `category_id`, `created_at`, `updated_at`, `description`) VALUES
(1, 'Hijab', 'hijab', '50000', '1', 1, '2021-01-10 09:59:53', '2021-01-10 09:59:53', '<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Magnam, dolor corrupti quis accusantium minus quasi et libero atque. Facere blanditiis cumque numquam veritatis voluptatibus iure amet odio voluptates itaque aspernatur.<br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quote` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `name`, `tanggal`, `quote`, `created_at`, `updated_at`) VALUES
(1, 'Hendrawan', '01-11-2020', 'Janganlah jadi orang bermuka dua, tapi carilah kawan bermuka dua. Saat kamu kehilangan muka, bisa pinjam satu ke kawanmu.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(2, 'Hendrawan', '02-11-2020', 'Mau gagal, mau sukses itu tidak penting yang penting berhasil.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(3, 'Hendrawan', '03-11-2020', 'Aku benci sama orang yang sok tau, banyak ngomong tapi gak ngerti. Makanya aku diam, supaya aku tidak membenci diriku sendiri.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(4, 'Hendrawan', '04-11-2020', 'Bila kamu jelek, jangan takut mencintai. Karena yang seharusnya takut adalah orang yang kamu cintai.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(5, 'Hendrawan', '05-11-2020', 'Cinta adalah pengorbanan, tapi kalo pengorbanan mulu sih namanya penderitaan.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(6, 'Hendrawan', '06-11-2020', 'Salah kalau banyak yang bilang hidup itu cuma satu kali. Yang benar, hidup itu setiap hari, dan mati hanya sekali.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(7, 'Hendrawan', '07-11-2020', 'Ketika kamu bilang biar Tuhan yang membalas, yang harus kamu tau adalah bahwa yang kamu alami sekarang adalah balasan dari Tuhan.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(8, 'Hendrawan', '08-11-2020', 'Kesuksesan diraih karena usaha keras. Usaha keras berawal dari kemauan kuat. Kemauan kuat terdorong oleh cita-cita tinggi. Cita-cita tinggi berawal dari mimpi. Mimpi terjadi karena tidur. Maka kalau kamu ingin sukses mari tidur.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(9, 'Hendrawan', '09-11-2020', 'Kalau kamu cari yang ganteng aku mundur, kalau kamu cari yang kaya, aku juga mundur, tapi kalau kamu cari yang pinter Matematika Akulator.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(10, 'Hendrawan', '10-11-2020', 'Kalau kamu stres memikirkan jalan keluar dari masalah kamu, kembalilah ke jalan masuk.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(11, 'Hendrawan', '11-11-2020', 'Pesan buat sesama perokok, jangan pernah takut mati kalau merokok, kan masih ada korek.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(12, 'Hendrawan', '12-11-2020', 'Jadikan masa lalu menjadi masa bodo.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(13, 'Hendrawan', '13-11-2020', 'Orang yang tidak pernah berjuang tidak akan pernah merasakan kalah', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(14, 'Hendrawan', '14-11-2020', 'Nyatanya, walau sering dianggap tidak higenis, makan pinggir jalan lebih aman daripada makan di tengah jalan.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(15, 'Hendrawan', '15-11-2020', 'Orang tua yang baik adalah yang memberi contoh kepada anaknya. Orang tua yang bijak adalah bisa belajar dari anaknya', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(16, 'Hendrawan', '16-11-2020', 'Wanita yang cantik bukanlah jaminan untuk hidup bahagia dan menyenangkan apalagi yang jelek.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(17, 'Hendrawan', '17-11-2020', 'Seberat apapun pekerjaan akan terasa ringan jika tidak dikerjakan.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(18, 'Hendrawan', '18-11-2020', 'Yang paling penting bagi seorang Pemimpin adalah \'N\' Karena tanpa \'N\' Pemimpin hanyalah Pemimpi.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(19, 'Hendrawan', '19-11-2020', 'Salah kalau banyak yang bilang hidup hanya sekali, yang benar hidup itu setiap hari dan mati hanya sekali.', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(20, 'Hendrawan', '20-11-2020', 'Kalau Cinta tidak bisa memiliki untuk apa diperjuangkan, itu adalah pikiran yang salah total, karena sebenarnya  ketika kita memilikipun bukan berarti perjuangan itu selesai.', '2021-01-10 07:52:24', '2021-01-10 07:52:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2021-01-10 07:52:23', '2021-01-10 07:52:23'),
(2, 'Admin', '2021-02-01 10:28:03', '2021-02-01 10:28:03');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `size`, `created_at`, `updated_at`) VALUES
(1, 'Kecil', 'S', '2021-01-10 09:14:47', '2021-01-10 09:14:47'),
(2, 'Sedang', 'M', '2021-01-10 09:15:19', '2021-01-10 09:15:19'),
(3, 'Besar', 'L', '2021-01-10 09:15:30', '2021-01-10 09:15:30');

-- --------------------------------------------------------

--
-- Table structure for table `size_products`
--

CREATE TABLE `size_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `size_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `size_products`
--

INSERT INTO `size_products` (`id`, `size_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-01-10 09:59:53', '2021-01-10 09:59:53'),
(2, 2, 1, '2021-01-10 09:59:53', '2021-01-10 09:59:53');

-- --------------------------------------------------------

--
-- Table structure for table `submenus`
--

CREATE TABLE `submenus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `submenu` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `submenus`
--

INSERT INTO `submenus` (`id`, `menu_id`, `submenu`, `url`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Beranda', '/dashboard/homes/index', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(2, 2, 'Postingan', '/dashboard/posts/post', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(3, 2, 'Kategori', '/dashboard/posts/category', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(4, 2, 'Tag', '/dashboard/posts/tag', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(5, 2, 'Komentar', '/dashboard/posts/comment', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(6, 3, 'Menu Management', '/dashboard/managements/menu', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(7, 3, 'Submenu Management', '/dashboard/managements/submenu', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(8, 3, 'Role Permission', '/dashboard/managements/role', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(9, 4, 'Daftar Pengguna', '/dashboard/users/index', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(10, 5, 'Umum', '/dashboard/settings/general', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(11, 5, 'Profil Anda', '/dashboard/settings/profile', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(12, 5, 'Keamanan', '/dashboard/settings/password', '1', '2021-01-10 07:52:24', '2021-01-10 07:52:24'),
(13, 6, 'Data Produk', '/dashboard/products/product', '1', '2021-01-10 08:00:14', '2021-01-10 08:03:01'),
(14, 6, 'Warna Produk', '/dashboard/products/color', '1', '2021-01-10 08:00:44', '2021-01-10 08:00:44'),
(15, 6, 'Ukuran Produk', '/dashboard/products/size', '1', '2021-01-10 08:01:06', '2021-01-10 08:01:06'),
(16, 7, 'Data Pesanan', '/dashboard/orders/order', '1', '2021-01-10 08:01:32', '2021-01-10 08:02:18'),
(17, 8, 'Data Pembayaran', '/dashboard/payments/payment', '1', '2021-01-10 08:01:57', '2021-01-10 08:01:57'),
(18, 6, 'Kategori Produk', '/dashboard/products/category', '1', '2021-01-10 09:16:48', '2021-01-10 09:16:48');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `avatar`, `telepon`, `address`, `instagram`, `facebook`, `youtube`, `twitter`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`, `google_id`) VALUES
(1, 'Hendrawan', 'hendrareansha@gmail.com', 'https://lh3.googleusercontent.com/a-/AOh14Ggip-ifW3ieYpt2MSONPUVROCu7eZQ1ydNsveiAbQ=s96-c', '+62 3982-2390-2399', 'Bondowoso', NULL, NULL, NULL, NULL, NULL, '$2y$10$.wsQNK0EY9HaOShBb03iM.UvIp7W7dJzwVWUlzS9kg1wLY5eQd.SG', NULL, '2021-01-10 07:52:24', '2021-01-10 07:52:24', 1, '113944289247095438178'),
(2, 'Administrator', 'admin@qonita.id', 'postImage/J472VdZNXer2qKjo.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$ARuCE/vT5XjOIksC2U/BleQ1MyyUsQppKxF5AZ3sHsa/nhSeAyAYW', NULL, '2021-02-01 10:29:26', '2021-02-01 10:30:19', 2, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_menus`
--
ALTER TABLE `access_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_menus_role_id_foreign` (`role_id`),
  ADD KEY `access_menus_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `access_submenus`
--
ALTER TABLE `access_submenus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_submenus_role_id_foreign` (`role_id`),
  ADD KEY `access_submenus_submenu_id_foreign` (`submenu_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_products`
--
ALTER TABLE `category_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color_products`
--
ALTER TABLE `color_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `color_products_color_id_foreign` (`color_id`),
  ADD KEY `color_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_payment_id_foreign` (`payment_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_items_product_id_foreign` (`product_id`),
  ADD KEY `order_items_order_id_foreign` (`order_id`),
  ADD KEY `size_id` (`size_id`),
  ADD KEY `color_id` (`color_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo_products`
--
ALTER TABLE `photo_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photo_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tags_post_id_foreign` (`post_id`),
  ADD KEY `post_tags_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `size_products`
--
ALTER TABLE `size_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `size_products_size_id_foreign` (`size_id`),
  ADD KEY `size_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `submenus`
--
ALTER TABLE `submenus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `submenus_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_menus`
--
ALTER TABLE `access_menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `access_submenus`
--
ALTER TABLE `access_submenus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_products`
--
ALTER TABLE `category_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `color_products`
--
ALTER TABLE `color_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `generals`
--
ALTER TABLE `generals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `photo_products`
--
ALTER TABLE `photo_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_tags`
--
ALTER TABLE `post_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `size_products`
--
ALTER TABLE `size_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `submenus`
--
ALTER TABLE `submenus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `access_menus`
--
ALTER TABLE `access_menus`
  ADD CONSTRAINT `access_menus_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `access_menus_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `access_submenus`
--
ALTER TABLE `access_submenus`
  ADD CONSTRAINT `access_submenus_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `access_submenus_submenu_id_foreign` FOREIGN KEY (`submenu_id`) REFERENCES `submenus` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `color_products`
--
ALTER TABLE `color_products`
  ADD CONSTRAINT `color_products_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `color_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `photo_products`
--
ALTER TABLE `photo_products`
  ADD CONSTRAINT `photo_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD CONSTRAINT `post_tags_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `post_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category_products` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `size_products`
--
ALTER TABLE `size_products`
  ADD CONSTRAINT `size_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `size_products_size_id_foreign` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `submenus`
--
ALTER TABLE `submenus`
  ADD CONSTRAINT `submenus_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
